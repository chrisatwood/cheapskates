package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/controllers"
	"bitbucket.org/chrisatwood/cheapskates/middleware"
)

// StartAdmin sets all admins endpoints
func StartAdmin() {
	admin := router.Group("admin")

	admin.GET("/", middleware.Authenication(), controllers.AdminIndex)
	admin.GET("/crawls/search/", middleware.Authenication(), controllers.AdminCrawls)
	admin.GET("/crawls/search/:page/", middleware.Authenication(), controllers.AdminCrawls)
	admin.GET("/crawls/search/:page/:total/", middleware.Authenication(), controllers.AdminCrawls)
	admin.GET("/crawls/view/:uuid/", middleware.Authenication(), controllers.AdminCrawlView)
}
