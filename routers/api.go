package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/controllers"
)

// APIRouters sets all API endpoints
func APIRouters() {
	v1 := router.Group("api/v1/")
	v1.GET("crawl/start/", controllers.APICrawlStart)
	v1.GET("crawl/results/", controllers.APICrawlResults)
	v1.GET("crawl/status/", controllers.APICrawlStatus)
	v1.GET("crawl/failures/", controllers.APICrawlFailures)
	v1.GET("crawl/controls/", controllers.APICrawlControls)
	v1.GET("crawl/cancel/:uuid", controllers.APICrawlCancel)
	v1.GET("crawl/fail-reasons/:uuid/:page/:total/", controllers.APICrawlFailReasons)
	v1.GET("crawl/items/:uuid/:page/:total/", controllers.APICrawlItems)

	v1.GET("products/:page/:total/", controllers.APIProducts)
}
