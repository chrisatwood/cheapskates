package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/start"
	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
)

var router *gin.Engine
var url string

// Start will init all routers
func Start() {
	// START ENV
	envy.Load(".env.prod")
	// END ENV - Automated
	
	runmode := envy.Get("RUNMODE", "debug")

	if runmode == "development" {
		url = "https://127.0.0.1"
	} else {
		gin.SetMode(gin.ReleaseMode)
		url = "https://www.cheapskates.info"
	}

	start.StartDB()
	start.Schedule()

	router = gin.Default()
	httpRouter := gin.Default()

	httpRouter.GET("/*path", func(c *gin.Context) {
		c.Redirect(302, url+"/"+c.Param("variable"))
	})
	
	router.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
		Root:      "templates",
		Extension: ".html",
		Master:    "layouts/master",
		Partials:  []string{},
	})

	
	StartAdmin()
	StartAuth()
	APIRouters()
	StartPublic()
	ServeStatic()

	router.Use(gin.Recovery())

	if runmode == "development" {
		go router.RunTLS(":443", "./certs/file.crt", "./certs/file.key")
		httpRouter.Run(":80")
	} else {
		go router.RunTLS(":443", "/etc/letsencrypt/live/cheapskates.info/cert.pem", "/etc/letsencrypt/live/cheapskates.info/privkey.pem")
		httpRouter.Run(":80")
	}
}
