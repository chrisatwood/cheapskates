package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/middleware"
	"bitbucket.org/chrisatwood/cheapskates/controllers"
	"github.com/gin-contrib/gzip"
)

// StartPublic sets all public endpoints
func StartPublic() {
	public := router.Group("/")
	public.GET("/", controllers.ProductSearchController)
	public.GET("/offline/", controllers.OfflineController)
	public.GET("/products/:page/:total/", controllers.ProductSearchController)
	public.GET("/product/view/:uuid/", controllers.ProductViewController)

	router.NoRoute(controllers.PageNotFoundController)
	router.Use(middleware.NoRouteCheck(), gzip.Gzip(gzip.DefaultCompression))
}
