package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/middleware"
)

// ServeStatic will serve all assets
func ServeStatic() {
	s := router.Group("./assets")
	s.Use(middleware.Cors())

	s.Static("/", "./assets/dist/")
}
