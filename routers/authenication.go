package routers

import (
	"bitbucket.org/chrisatwood/cheapskates/controllers"
	"bitbucket.org/chrisatwood/cheapskates/middleware"
)

// StartAuth Sets all authenication endpoints
func StartAuth() {
	auth := router.Group("auth")
	auth.GET("create/", middleware.NotAuthenicated(), controllers.CreateUser)
	auth.GET("login/", middleware.NotAuthenicated(), controllers.AdminLogin)
	auth.POST("login/", controllers.AdminLoginPost)
	auth.GET("logout", middleware.Authenication(), controllers.AdminLogout)
}
