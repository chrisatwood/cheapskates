package main

import (
	"bitbucket.org/chrisatwood/cheapskates/routers"
)

func main() {
	routers.Start()
}
