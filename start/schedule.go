package start

import (
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"bitbucket.org/chrisatwood/cheapskates/models"
	"github.com/gobuffalo/envy"
	"github.com/robfig/cron"
)

// Schedule will add a cron job to crawl
func Schedule() {
	cron := cron.New()

	// Add function to add crawler
	cron.AddFunc(envy.Get("CRAWLINTERVAL", "@daily"), func() {

		urls := strings.Split(envy.Get("CRAWLSTARTURLS", "https://www.ebuyer.com/"), ";")

		crawl := models.Crawl{}
		crawl.Settings.Limit = uint(helpers.ParseInt(envy.Get("CRAWLITEMSLIMIT", "122")))
		crawl.Settings.ConcurrentDelay = uint(helpers.ParseInt(envy.Get("CRAWLDELAY", "250")))
		crawl.Settings.Units = uint(helpers.ParseInt(envy.Get("CRAWLUNITS", "3")))

		crawl.Init()

		for _, url := range urls {
			crawlURL := models.CrawlBaseURL{
				Crawl: crawl,
				URL:   url,
			}
			crawlURL.Create()

			crawl.Settings.BaseURLs = append(crawl.Settings.BaseURLs, crawlURL)

		}

		crawl.SetBaseURLs()
		crawl.Begin()
	})
	cron.Start()
}
