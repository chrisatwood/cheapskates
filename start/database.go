package start

import (
	"log"

	"bitbucket.org/chrisatwood/cheapskates/models"
	"github.com/gobuffalo/envy"
	"github.com/jinzhu/gorm"
)

// StartDB a Craetes Tables and migrations when server is launched
func StartDB() {
	db, err := gorm.Open("mysql", envy.Get("DBUSER", "root")+":"+envy.Get("DBPASSWORD", "")+"@/"+envy.Get("DBNAME", "cheapskates_dev")+"?charset="+envy.Get("DBCHARSET", "utf8")+"&parseTime="+envy.Get("DBParseTime", "True")+"&loc="+envy.Get("DBLOC", "UTC"))
	if err != nil {
		log.Fatal("Unable to connect to DB", err)
	}

	models.UserAction{}.Migrate()
	models.User{}.Migrate()
	models.UserActivity{}.Migrate()
	models.Session{}.Migrate()
	models.Product{}.Migrate()
	models.ProductImage{}.Migrate()
	models.ProductPrice{}.Migrate()
	models.ProductRating{}.Migrate()
	models.ProductSeller{}.Migrate()
	models.ProductVariation{}.Migrate()
	models.Crawl{}.Migrate()
	models.CrawlBaseURL{}.Migrate()
	models.Proxy{}.Migrate()
	models.CrawlItemSkipped{}.Migrate()
	models.CrawlItem{}.Migrate()

	db.DB().SetMaxIdleConns(0)
	defer db.Close()
}
