tippy(".progress-bar")
const cancelBtn = document.querySelector("#cancelCrawl")
if (cancelBtn !== undefined && cancelBtn !== null) {
  cancelBtn.addEventListener("click", function (e) {
    if (this.attributes['data-uuid'].value.length > 0) {
      fetch(`https://127.0.0.1/api/v1/crawl/cancel/${this.attributes['data-uuid'].value}`)
        .then(res => location.reload())
    }
  })
}