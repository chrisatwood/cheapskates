document.querySelector("#menuBurger").addEventListener("click", e => {
  e.preventDefault()
  const sidebar = document.querySelector("#sideBar")
  const content = document.querySelector("#content")
  if (sidebar.classList.contains("sm:w-1/3")) {
    sidebar.classList.replace("sm:w-1/3", "w-0")
    sidebar.classList.remove("lg:w-1/5")
    content.classList.replace("sm:w-2/3", "w-full")
    content.classList.remove("lg:w-4/5")
    setTimeout(() => {
      sidebar.classList.remove("sm:visible")
    }, 301)
  } else {
    sidebar.classList.replace("w-0", "sm:w-1/3")
    sidebar.classList.add("lg:w-1/5")

    content.classList.replace("w-full", "sm:w-2/3")
    content.classList.add("lg:w-4/5")
    setTimeout(() => {
      sidebar.classList.add("sm:visible")
    }, 301)
  }
  console.log(e)
})