const hasCookie = () => {
  var name = "csc=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return true
    }
  }
  return false;
}

const setCookie = () => {
  return new Promise(resolve => {
    const d = new Date()

    d.setFullYear(d.getFullYear() + 1)
    document.cookie = `csc=1;expires=${d.toUTCString()}; path=/`
    resolve()
  })
}

const hide = () => {
  element.parentElement.parentElement.parentElement
    .style.transition = "opacity 200ms ease-in-out"
  element.parentElement.parentElement.parentElement
    .style.opacity = "0"
  setTimeout(() => {
    element.parentElement.parentElement.parentElement.remove()
  }, 201)
}

const exists = hasCookie()
const element = document.querySelector("#cookie-btn")

element.addEventListener('click', () => {
  if (!exists) {
    setCookie()
      .then(() => hide())
  } else {
    hide()
  }
})