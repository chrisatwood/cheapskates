class AutoComplete {


  constructor() {
    this.query = ""
    this.showingResults = false
    this.focus = false
    this.items = []
    this.template = {
      container: `
      <div id="search-autocomplete" class=" z-20 absolute w-full bg-white h-auto shadow p-2 flex flex-col transition">
        {{items}}
        {{showMore}}
      </div>`,
      item: `
      <a class="border border-t-0 border-r-0 border-l-0 mb-2 w-full flex p-2 items-center hover:bg-grey-lightest no-underline transition" href="{{productURL}}">
        {{image}}
        <p class="text-sm text-black w-full align-middle no-underline">{{productName}}</p>
      </a>`,
      noResults: `<p class="w-full flex p-2 items-center transition text-sm text-grey-dark  ">There are no products matching {{searchQuery}}</p>`,
      showMore: `<a class="w-full flex p-2 items-center hover:bg-grey-lightest no-underline transition text-sm text-grey-dark no-underline " href="{{searchURL}}">See all results for {{searchQuery}}</a>`
    }
    const element = document.querySelector("#search-query")
    const overlay = document.querySelector("#search-overlay")
    element.addEventListener("keyup", () => {
      this.query = element.value
      this.search()
      this.focus = true
    })
    overlay.addEventListener("click", () => {
      this.close()
    })
  }

  search() {
    const ac = this
    return new Promise(resolve => {
      fetch(`https://127.0.0.1/api/v1/products/1/10/?q=${this.query}`)
        .then(res => res.json())
        .then(data => {
          ac.items = data.Items
          ac.display()
        })
        .catch(e => console.log(e))
    })
  }

  display() {

    document.querySelector("#search-overlay").classList.remove("hidden")
    let container = this.template.container
      .replace("{{searchQuery}}", this.query)
    let itemsHTML = ''

    if (this.items === null) {
      itemsHTML = this.template.noResults.replace("{{searchQuery}}", this.query)
      container = container.replace("{{showMore}}", "")
    } else {
      container = container
        .replace("{{showMore}}", this.template.showMore.replace("{{searchQuery}}", this.query))
        .replace("{{searchURL}}", `https://127.0.0.1/products/1/12/?q=${this.query}`)

      const reg = new RegExp(this.query, 'gi');

      this.items.forEach(item => {
        let image = ''
        if (item.Images[0] !== undefined) {
          const split = item.Images[0].src.split("upload/")
          image = `<img src="${split[0] + 'upload/f_auto,fl_tiled,q_auto,w_25/' + split[1]}" class="w-6 mr-5">`
        } else {
          image = `<div class="w-6 mr-5"></div>`
        }
        item.Name = item.Name.replace(reg, '<strong>$&</strong>')

        itemsHTML += this.template.item.replace("{{productName}}", item.Name).replace("{{productURL}}", item.url).replace("{{image}}", image)
      })
    }
    const acc = document.querySelector("#search-autocomplete")

    if (acc !== null) {
      acc.remove()
    }
    document.querySelector("#search-query").insertAdjacentHTML('afterend', container.replace("{{items}}", itemsHTML))
  }
  close() {
    const acc = document.querySelector("#search-autocomplete")
    document.querySelector("#search-overlay").classList.add("hidden")

    if (acc !== null) {
      acc.remove()
    }

  }
}
const ac = new AutoComplete()