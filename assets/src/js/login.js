document.getElementById("loginForm").addEventListener("submit", function (e) {
  hasError = false;
  email = document.querySelector("#email");
  password = document.querySelector("#password");

  if (email.value.length <= 0) {
    emailErr = document.createElement("p");
    emailErr.classList.add("text-red");
    emailErr.classList.add("text-xs");
    emailErr.classList.add("itatlic");
    emailErr.innerHTML = config.errors.email;
    emailErr.id = "emailErr";
    email.after(emailErr);
    hasError = true;
  } else {
    err = document.querySelector("#emailErr");

    if (err != undefined) {
      err.remove();
    }
  }

  if (password.value.length <= 0) {
    passswordErr = document.createElement("p");
    passswordErr.classList.add("text-red");
    passswordErr.classList.add("text-xs");
    passswordErr.classList.add("itatlic");
    passswordErr.innerHTML = config.errors.password;
    passswordErr.id = "passwordError";

    password.after(passswordErr);
    hasError = true;
  } else {
    err = document.querySelector("#passwordError");

    if (err != undefined) {
      err.remove();
    }
  }

  if (hasError) {
    e.preventDefault();

  }
});