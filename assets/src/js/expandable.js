class Expand {
  constructor(_element) {
    this.element = _element
    this.open = false
    this.transition = false
    this.templateClass = 'bg-white fade-up absolute pin-b w-full text-right'
    this.templateBtnID = 'expand-btn'
    this.templateBtnClass = "p-3 mt-3 bg-blue text-white hover:bg-blue-light right"


    this.addExpandButton()
  }
  addExpandButton() {
    this.button = document.createElement("div")
    this.button.className = this.templateClass
    this.button.innerHTML = `<button class="${this.templateBtnClass}" id="${this.templateBtnID}">Expand</button>`

    this.element.appendChild(this.button)
  }
}

const items = document.querySelectorAll(".expandable")

items.forEach(item => {
  const i = new Expand(item)
  i.button.addEventListener("click", () => {

    i.button.remove()
    i.element.classList.remove("h-128")
    i.element.classList.remove("overflow-y-hidden")
  })
  console.log(i)
})