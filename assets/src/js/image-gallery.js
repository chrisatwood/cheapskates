class ImageGallery {
  constructor() {
    this.thumbnails = document.getElementsByClassName("image-thumbnail")
    this.main = document.querySelector("#image-gallery #image-main")

    Array.from(this.thumbnails).forEach(item => {
      item.addEventListener("click", e => {
        this.handler(e)
      })
    })
  }

  handler(item) {
    document.querySelector(".image-thumbnail.border-blue").classList.remove('border-blue')
    item.target.classList.add("border-blue")
    this.main.setAttribute("src", item.target.attributes['data-gallery-src'].value)
    this.main.setAttribute("srcset", item.target.attributes['data-gallery-srcset'].value)

  }
}
const ig = new ImageGallery()