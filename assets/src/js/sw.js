importScripts('/assets/js/cacheall-polyfill.js')

const version = "0.2.1";
const cacheName = `cheapskates-${version}`;
const OFFLINE_URL = `/offline/`
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        OFFLINE_URL,
        `/`,
        `/assets/css/main.css`,
        `/assets/js/menu.js`,
        `/assets/images/favicon-32x32.png`,
        `/assets/images/favicon-16x16.png`,
        `/assets/images/logo.png`
      ])
        .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', function (e) {
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (cacheName.indexOf(key) === -1) {
          return caches.delete(key);
        }
      }));
    })
  );
});
self.addEventListener('fetch', function (event) {
  if (event.request.mode === 'navigate' ||
    (event.request.method === 'GET' &&
      event.request.headers.get('accept').includes('text/html'))) {

    event.respondWith(
      fetch(event.request).catch(error => {
        // The catch is only triggered if fetch() throws an exception, which will most likely
        // happen due to the server being unreachable.
        // If fetch() returns a valid HTTP response with an response code in the 4xx or 5xx
        // range, the catch() will NOT be called. If you need custom handling for 4xx or 5xx
        // errors, see https://github.com/GoogleChrome/samples/tree/gh-pages/service-worker/fallback-response
        console.log('Fetch failed; returning offline page instead.', error);
        return caches.match(OFFLINE_URL);
      })
    )
  }
});