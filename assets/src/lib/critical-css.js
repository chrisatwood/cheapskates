const critical = require('critical');
const fs = require('fs');
const path = require('path');
const base = '../dist/css/above-fold/'
const ext = '.css'
const pageTypes = [{ url: "index", name: 'homepage' }, { url: "404", name: '404' }, { url: "product", name: "product-page" }, { url: "auth/login", name: 'auth/login' }, { url: "admin/index", name: 'admin/index' }, { url: "admin/crawl/index", name: 'admin/crawl' }, { url: "admin/crawl/item", name: 'admin/crawl-items' }]

pageTypes.forEach((value, index) => {
  console.log(value)
  const dest = `${base}${value.name}${ext}`
  critical.generate({
    base: '../dist',
    src: `../src/html/${value.url}.html`,
    minify: true,
    width: 1300,
    height: 900
  }).then(function (output) {
    // You now have critical-path CSS
    // Works with and without dest specified

    var dirname = path.dirname(dest);
    if (!fs.existsSync(dirname)) {
      fs.mkdir(dirname, () => { })
    }

    fs.writeFileSync(dest, output, (err) => {
      if (err) throw err;
    });
  }).error(function (err) {
    console.log(value, err)
  });
})