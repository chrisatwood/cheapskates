"use strict";/**
   * Copyright 2015 Google Inc. All rights reserved.
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   *     http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   *
   */(function(){var a=Cache.prototype.addAll,b=navigator.userAgent.match(/(Firefox|Chrome)\/(\d+\.)/);// Has nice behavior of `var` which everyone hates
if(b)var c=b[1],d=parseInt(b[2]);a&&(!b||"Firefox"===c&&46<=d||"Chrome"===c&&50<=d)||(Cache.prototype.addAll=function(a){// Since DOMExceptions are not constructable:
function b(a){this.name="NetworkError",this.code=19,this.message=a}var c=this;return b.prototype=Object.create(Error.prototype),Promise.resolve().then(function(){if(1>arguments.length)throw new TypeError;// Simulate sequence<(Request or USVString)> binding:
return a=a.map(function(a){return a instanceof Request?a:a+""}),Promise.all(a.map(function(a){"string"==typeof a&&(a=new Request(a));var c=new URL(a.url).protocol;if("http:"!==c&&"https:"!==c)throw new b("Invalid scheme");return fetch(a.clone())}))}).then(function(d){// If some of the responses has not OK-eish status,
// then whole operation should reject
if(d.some(function(a){return!a.ok}))throw new b("Incorrect response status");// TODO: check that requests don't overwrite one another
// (don't think this is possible to polyfill due to opaque responses)
return Promise.all(d.map(function(b,d){return c.put(a[d],b)}))}).then(function(){})},Cache.prototype.add=function(a){return this.addAll([a])})})();
//# sourceMappingURL=cacheall-polyfill.js.map