package models

import (
	"time"

	"github.com/gobuffalo/envy"

	"bitbucket.org/chrisatwood/cheapskates/database"
	uuid "github.com/google/uuid"
)

// CrawlSearch extends Search and returns all search items
type CrawlSearch struct {
	Search
	Items []CrawlSearchItem
}

// CrawlSearchItem contains basic information on a item
type CrawlSearchItem struct {
	UUID      uuid.UUID `json:"uuid"`
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	Status    struct {
		IsCrawling bool `json:"is_crawling"`
	} `gorm:"embeded"`
	Elapsed       float64 `json:"elapsed"`
	ElapsedFormat string  `json:"elapsed_format"`
}

// Filter sets the information to return in the items field.
func (crawlSearch *CrawlSearch) Filter() {
	db := database.CreateDBConnection()

	// Check for valid arugment
	if crawlSearch.Pagination.CurrentPage < 1 {
		crawlSearch.Search.IsInvalid()
	} else {
		db.Find(&[]Crawl{}).Count(&crawlSearch.Results.Total)

		if crawlSearch.Results.Total <= 0 {
			crawlSearch.Search.IsInvalid()
		} else {
			db.Table(Crawl{}.TableName()).
				Order("start_time DESC").
				Offset((crawlSearch.Pagination.CurrentPage - 1) * crawlSearch.Query.ItemsPerPage).
				Select("uuid, start_time, end_time, is_crawling, elapsed").
				Limit(crawlSearch.Query.ItemsPerPage).
				Scan(&crawlSearch.Items)

			crawlSearch.Results.TotalReturned = len(crawlSearch.Items)
			crawlSearch.Search.SetPagination(envy.Get("URLSADMINCRAWLS", "/"), "")
			crawlSearch.Search.SetResults()
			for i := range crawlSearch.Items {
				crawlSearch.Items[i].ElapsedFormat = time.Duration(time.Duration(crawlSearch.Items[i].Elapsed) * time.Second).String()
			}
		}
	}
	database.CloseDBConnection(db)
}
