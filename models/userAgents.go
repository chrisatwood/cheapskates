package models

import (
	"encoding/json"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
)

// UserAgent holds UA information
type UserAgent struct {
	Commonality string `json:"commonality"`
	Version     string `json:"version"`
	UA          string `json:"us"`
}

// UserAgents holds many UAs
type UserAgents []UserAgent

// Load loads all UAs
func (UAs *UserAgents) Load() {
	userAgentsFile := helpers.ReadFile("./config/userAgents.json")

	jsonParser := json.NewDecoder(userAgentsFile)
	jsonParser.Decode(&UAs)
	userAgentsFile.Close()
}

// Random assigns a random UA
func (UAs UserAgents) Random() UserAgent {
	return UAs[helpers.GetRandomInt(len(UAs)-1)]
}

// String converts to string
func (ua UserAgent) String() string {
	return ua.UA
}
