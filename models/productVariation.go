package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// ProductVariation holds information regarding the product variation
type ProductVariation struct {
	DefaultModel
	ProductID uint `gorm:"default:null"`
	Key       string
	Value     string
}

// Migrate sets all foreign keys
func (variation ProductVariation) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&variation)
	db.Model(variation).AddForeignKey("product_id", database.GetDBTableNameAndFeild([]string{"PRODUCTS"}, "id"), "RESTRICT", "RESTRICT")

	database.CloseDBConnection(db)
}

// TableName changes ProductVariations table name
func (ProductVariation) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTSPREFIX", "") + envy.Get("PRODUCTVARIATIONS", "")
}

// AfterCreate add uuid to ProductVariations record
func (variation *ProductVariation) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(variation).Update("uuid", uuid.New())
	return
}
