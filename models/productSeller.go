package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// The ProductSeller contains the name and URL of the seller.
type ProductSeller struct {
	DefaultModel
	ProductID uint
	RatingID  uint
	Rating    ProductRating `gorm:"foreignkey:RatingID"`
	Name      string        `gorm:"type:varchar(100)"`
	URL       string        `gorm:"type:text"`
	Product   Product       `gorm:"foreignkey:ProductID"`
}

// Migrate sets all foreign keys
func (seller ProductSeller) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&seller)
	db.Model(&seller).AddForeignKey("product_id", database.GetDBTableNameAndFeild([]string{"PRODUCTS"}, "id"), "RESTRICT", "RESTRICT")
	db.Model(&seller).AddForeignKey("rating_id", database.GetDBTableNameAndFeild([]string{"PRODUCTSPREFIX", "PRODUCTRATINGS"}, "id"), "RESTRICT", "RESTRICT")

	database.CloseDBConnection(db)
}

// TableName changes ProductSellers table name
func (ProductSeller) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTSPREFIX", "") + envy.Get("PRODUCTSELLERS", "")
}

// AfterCreate Adds uuid to ProductSellers record
func (seller *ProductSeller) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(seller).Update("uuid", uuid.New())
	return
}

// Exists will return if the seller already exists
func (seller ProductSeller) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("product_id = ? AND name = ?", seller.ProductID, seller.Name).Find(&seller).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (seller *ProductSeller) Create() {
	db := database.CreateDBConnection()
	db.Create(&seller)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (seller *ProductSeller) Update() {
	db := database.CreateDBConnection()
	db.Save(&seller)
	database.CloseDBConnection(db)
}
