package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// UserAction holds different types of action a user could perform
type UserAction struct {
	DefaultModel
	Name        string
	Description string `gorm:"type:text"`
}

// Migrate sets all foreign keys
func (userAction UserAction) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&userAction)
	database.CloseDBConnection(db)
}

// TableName changes UserAction table name
func (UserAction) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("ADMINPREFIX", "") + envy.Get("USERSPREFIX", "") + envy.Get("USERACTIONS", "")
}

//AfterCreate adds uuid to user action record
func (userAction *UserAction) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(userAction).Update("uuid", uuid.New())
	return
}

// Create will create a new DB item
func (userAction *UserAction) Create() {
	db := database.CreateDBConnection()
	db.Create(&userAction)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (userAction *UserAction) Update() {
	db := database.CreateDBConnection()
	db.Save(&userAction)
	database.CloseDBConnection(db)
}

// Exists will return if the product already exists
func (userAction *UserAction) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("name = ?", userAction.Name).First(&userAction).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}
