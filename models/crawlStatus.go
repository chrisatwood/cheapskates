package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
)

// CrawlStatus holds crawlItem statuses
type CrawlStatus struct {
	CrawlStatusItems []CrawlStatusItem `json:"items"`
}

// CrawlStatusItem holds status of a crawlItem
type CrawlStatusItem struct {
	UUID           uuid.UUID `json:"uuid"`
	Date           string    `json:"date"`
	IsCrawling     bool      `json:"is_crawling"`
	IsComplete     bool      `json:"is_completed"`
	IsFailed       bool      `json:"is_failed"`
	IsCancelled    bool      `json:"is_cancelled"`
	TotalCompleted uint      `json:"total_crawled"`
}

// Get will set all statuses
func (crawlStatus *CrawlStatus) Get() {
	var crawls []Crawl
	db := database.CreateDBConnection()

	notFound := db.Order("start_time DESC").Limit(helpers.ParseInt(envy.Get("ADMINTOTALSTATUS", "5"))).Find(&crawls).RecordNotFound()
	database.CloseDBConnection(db)

	if !notFound {
		for _, crawl := range crawls {
			crawlItems := CrawlItems{
				CrawlID: crawl.ID,
			}
			crawlItems.Results()

			crawlStatusItem := CrawlStatusItem{
				UUID:           crawl.UUID,
				Date:           crawl.StartTime.Format("2006-01-02 15:04:05"),
				IsCrawling:     crawl.Status.IsCrawling,
				IsComplete:     crawl.Status.IsFinished,
				IsCancelled:    crawl.Status.IsCancelled,
				IsFailed:       crawl.HasFailed(),
				TotalCompleted: crawlItems.TotalCompleted,
			}

			crawlStatus.CrawlStatusItems = append(crawlStatus.CrawlStatusItems, crawlStatusItem)
		}
	}
}
