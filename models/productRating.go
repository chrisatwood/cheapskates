package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// ProductRating contain the number of stars, the max number of
// stars available and the product reviews.
type ProductRating struct {
	DefaultModel
	Stars           float32
	MaxStars        int16
	NumberOfReviews int32
	URL             string `gorm:"type:text"`
}

// Migrate sets all foreign keys
func (rating ProductRating) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&rating)
	database.CloseDBConnection(db)
}

// TableName changes ProductRatings table name
func (ProductRating) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTSPREFIX", "") + envy.Get("PRODUCTRATINGS", "")
}

// AfterCreate add uuid to  ProductRatings record
func (rating *ProductRating) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(rating).Update("uuid", uuid.New())
	return
}

// Exists will return if the rating already exists
func (rating ProductRating) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("url = ?", rating.URL).Find(&rating).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (rating *ProductRating) Create() {
	db := database.CreateDBConnection()
	db.Create(&rating)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (rating *ProductRating) Update() {
	db := database.CreateDBConnection()
	db.Save(&rating)
	database.CloseDBConnection(db)
}
