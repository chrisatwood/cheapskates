package models

import (
	"time"

	"github.com/google/uuid"
)

// DefaultModel provides all default fields for any record
type DefaultModel struct {
	ID        uint `gorm:"primary_key" json:"-"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt *time.Time `sql:"index" json:"deleted_at"`
	UUID      uuid.UUID  `gorm:"type:varchar(128)" json:"uuid"`
}