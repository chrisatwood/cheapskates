package models

// ParseToplevel will parse a top level domain
func (parser *Parser) ParseToplevel() {
	// Process all urls on this page
	parser.GetPageURLs()
	// Process all urls on this page
	parser.Crawl.ProcessPageURLs(parser.Pages)

	parser.CrawlItem.Status.IsProcessing = false
	parser.CrawlItem.Status.IsComplete = true

	parser.CrawlItem.Update()
}
