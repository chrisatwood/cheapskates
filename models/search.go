package models

import (
	"fmt"
	"math"
	"time"
)

// Search is the base for all search pages
type Search struct {
	Query struct {
		Query        string    `json:"search"`
		DateRaw      string    `json:"raw_date"`
		Date         time.Time `json:"date"`
		ItemsPerPage int       `json:"item_per_page"`
	} `json:"query"`
	Pagination struct {
		StartPage   int              `json:"start_page"`
		LastPage    int              `json:"last_page"`
		TotalPages  int              `json:"total_pages"`
		CurrentPage int              `json:"current_page"`
		Pages       []PaginationPage `json:"pages"`
		Shown       bool             `json:"is_shown"`
		Navigation  struct {
			Back struct {
				Page     int    `json:"page"`
				PageURL  string `json:"page_url"`
				Disabled bool   `json:"disabled" default:"false"`
			} `json:"back"`
			Next struct {
				Page     int    `json:"page"`
				PageURL  string `json:"page_url"`
				Disabled bool   `json:"disabled" default:"false"`
			} `json:"next"`
		} `json:"navigation"`
	} `json:"pagination"`
	Results struct {
		HasResults    bool `json:"has_results"`
		TotalReturned int  `json:"total_returned"`
		Total         int  `json:"total"`
		StartItem     int  `json:"start_item"`
		LastItem      int  `json:"last_item"`
	} `json:"results"`
}

//PaginationPage contains information regarding a page within a pagination
type PaginationPage struct {
	Active  bool   `json:"is_active"`
	Page    int    `json:"page"`
	PageURL string `json:"page_url"`
}

// IsInvalid sets the results to be invalid
func (search *Search) IsInvalid() {
	search.Results.HasResults = false
	search.Pagination.Shown = false
}

// SetPagination sets the pagination pages and navigation
func (search *Search) SetPagination(path string, query string) {
	search.Pagination.TotalPages = int(math.Ceil(float64(search.Results.Total) / float64(search.Query.ItemsPerPage)))

	if search.Pagination.TotalPages == 1 {
		search.Pagination.Shown = false
	} else {
		search.Pagination.Shown = true
		search.Pagination.StartPage = 1

		if search.Pagination.CurrentPage == 1 {
			search.Pagination.Navigation.Back.Disabled = true
		} else {
			search.Pagination.Navigation.Back.Page = search.Pagination.CurrentPage - 1
			search.Pagination.Navigation.Back.PageURL = search.GetURL(search.Pagination.CurrentPage-1, path, query)
		}

		if search.Pagination.CurrentPage > 5 {
			search.Pagination.StartPage = search.Pagination.CurrentPage - 5
		}

		search.Pagination.LastPage = search.Pagination.CurrentPage + 5

		if search.Pagination.LastPage >= search.Pagination.TotalPages {
			search.Pagination.LastPage = search.Pagination.TotalPages
		}

		if search.Pagination.CurrentPage == search.Pagination.TotalPages {
			search.Pagination.Navigation.Next.Disabled = true
		} else {
			search.Pagination.Navigation.Next.Page = search.Pagination.CurrentPage + 1
			search.Pagination.Navigation.Next.PageURL = search.GetURL(search.Pagination.CurrentPage+1, path, query)
		}

		for i := search.Pagination.StartPage; i <= search.Pagination.LastPage; i++ {
			search.Pagination.Pages = append(search.Pagination.Pages, PaginationPage{Page: i, PageURL: search.GetURL(i, path, query), Active: (i == search.Pagination.CurrentPage)})
		}
	}

}

// SetResults will set the start item and last item of the indexed list
func (search *Search) SetResults() {
	if search.Results.TotalReturned > 0 {
		search.Results.HasResults = true

		search.Results.StartItem = (search.Pagination.CurrentPage-1)*search.Query.ItemsPerPage + 1
		search.Results.LastItem = (search.Results.StartItem + search.Results.TotalReturned) - 1

		if search.Results.Total < (search.Results.StartItem+search.Results.TotalReturned)-1 {
			search.Results.LastItem = search.Results.Total
		}
	} else {
		search.IsInvalid()
	}
}

// GetURL returns a relative URL for a given page index and query per page
func (search Search) GetURL(index int, path string, query string) string {
	return fmt.Sprintf(path+"%d/%d/?%s", index, search.Query.ItemsPerPage, query)
}
