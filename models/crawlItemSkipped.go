package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// CrawlItemSkipped holds inforamtion regarding a skipped crawl.
type CrawlItemSkipped struct {
	DefaultModel
	Slug        string `gorm:"type:varchar(150)" json:"slug"`
	Description string `gorm:"type:text" json:"description"`
}

// Migrate sets all foreign keys
func (crawlsItemSkipped CrawlItemSkipped) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&crawlsItemSkipped)
	database.CloseDBConnection(db)
}

// TableName changes CrawlItemSkipped table name
func (CrawlItemSkipped) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("CRAWLSPREFIX", "") + envy.Get("CRAWLITEMSPREFIX", "") + envy.Get("CRAWLITEMSKIPPED", "")
}

// AfterCreate Adds uuid to crawlItem skipped record
func (crawlsItemSkipped *CrawlItemSkipped) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(crawlsItemSkipped).Update("uuid", uuid.New())
	return
}

// Exists will return if the image already exists
func (crawlsItemSkipped *CrawlItemSkipped) Exists() bool {
	db := database.CreateDBConnection()
	var notFound bool
	if crawlsItemSkipped.Description == "" {
		notFound = db.Where("slug = ?", crawlsItemSkipped.Slug).First(&crawlsItemSkipped).RecordNotFound()
	} else {
		notFound = db.Where("slug = ? AND description = ?", crawlsItemSkipped.Slug, crawlsItemSkipped.Description).First(&crawlsItemSkipped).RecordNotFound()
	}
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new CrawlItem in the DB
func (crawlsItemSkipped *CrawlItemSkipped) Create() {
	db := database.CreateDBConnection()
	db.Create(&crawlsItemSkipped)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (crawlsItemSkipped *CrawlItemSkipped) Update() {
	db := database.CreateDBConnection()
	db.Save(&crawlsItemSkipped)
	database.CloseDBConnection(db)
}
