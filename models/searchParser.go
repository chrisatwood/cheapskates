package models

import (
	"bitbucket.org/chrisatwood/cheapskates/helpers"
)

// ParseSearch parses a search page and retrevie all relevent information
func (parser *Parser) ParseSearch() {
	// helpers.AddLog("Parsing search page", parser.Crawl.LogName, parser.URL.String())
	parser.GetNumberOfSearchPages()
	//Set search page URLs
	parser.SetSearchPages()
	// Add product page items
	parser.SetProductPages()

	parser.Crawl.ProcessPageURLs(parser.Pages)

	parser.CrawlItem.Status.IsProcessing = false
	parser.CrawlItem.Status.IsComplete = true

	parser.CrawlItem.Update()
}

// GetNumberOfSearchPages returns total number of pages of a search
func (parser *Parser) GetNumberOfSearchPages() {
	var total string
	var current string

	totalDOM := parser.Document.Find(parser.Parser.DOMElements.Search.TotalPages.Selector)
	currentDOM := parser.Document.Find(parser.Parser.DOMElements.Search.CurrentPage.Selector)

	if parser.Parser.DOMElements.Search.TotalPages.Attribute != "null" {
		attr, _ := totalDOM.Attr(parser.Parser.DOMElements.Search.TotalPages.Attribute)
		total = attr
	} else {
		total = totalDOM.Text()
	}

	if parser.Parser.DOMElements.Search.CurrentPage.Attribute != "null" {
		attr, _ := currentDOM.Attr(parser.Parser.DOMElements.Search.CurrentPage.Attribute)
		current = attr
	} else {
		current = currentDOM.Text()
	}

	if parser.Parser.Regex.Search.TotalPages != "null" {
		total = helpers.RegexReplaceSubString(total, parser.Parser.Regex.Search.TotalPages, "")
	}

	if parser.Parser.Regex.Search.CurrentPage != "null" {
		current = helpers.RegexReplaceSubString(current, parser.Parser.Regex.Search.TotalPages, "")
	}

	parser.CurrentPage = helpers.ParseInt8(current)
	parser.TotalPages = helpers.ParseInt8(total)
}
