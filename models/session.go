package models

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)
// Session holds cookie information
type Session struct {
	DefaultModel
	UserActivityID uint         `json:"-"`
	UserActivity   UserActivity `gorm:"foreignkey:UserActivityID"`
	Hash           string       `json:"-"`
	Expires        time.Time
}

// Migrate sets all foreign keys
func (session Session) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&session)
	db.Model(&session).AddForeignKey("user_activity_id", database.GetDBTableNameAndFeild([]string{"ADMINPREFIX", "USERSPREFIX", "USERACTIVITIES"}, "id"), "RESTRICT", "RESTRICT")

	database.CloseDBConnection(db)
}

// TableName changes Session table name
func (Session) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("ADMINPREFIX", "") + envy.Get("USERSPREFIX", "") + envy.Get("USERSESSIONS", "")
}

//AfterCreate adds uuid to user activity record
func (session *Session) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(session).Update("uuid", uuid.New())
	return
}

// Exists will return if the product already exists
func (session *Session) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("hash = ? AND CURRENT_TIME >= expires", session.Hash).First(&session).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (session *Session) Create() {
	db := database.CreateDBConnection()
	session.Expires = time.Now().Add(30 * time.Minute)
	db.Create(&session)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (session *Session) Update() {
	db := database.CreateDBConnection()

	session.Expires = time.Now().Add(30 * time.Minute)
	db.Save(&session)
	database.CloseDBConnection(db)
}

// Delete will update the record in the Database
func (session *Session) Delete() {
	db := database.CreateDBConnection()

	session.Expires = time.Now()
	db.Delete(&session)
	database.CloseDBConnection(db)
}
