package models

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	httpstat "github.com/tcnksm/go-httpstat"
)

// CrawlSearch extends Search and returns all search items
type CrawlItemsSearch struct {
	Search
	Items []CrawlItem `json:"items"`
}

// CrawlItemsSearchItem contains basic information on a item
type CrawlItemsSearchItem struct {
	UUID              uuid.UUID `json:"uuid"`
	CreatedAt         time.Time `json:"created_at"`
	ProcessingStarted time.Time `json:"processing_started"`
	ProcessingEnded   time.Time `json:"processing_ended"`
	Status            struct {
		IsProcessing bool `json:"is_processing"`
		IsSkipped    bool `json:"is_skipped"`
		IsComplete   bool `json:"is_complete"`
		IsQueued     bool `json:"is_queued"`
	} `gorm:"embedded" json:"status"`
	Timings struct {
		QueuedTime         int64           `gorm:"default:null" json:"queued_time"`
		Queued             float64         `gorm:"default:null" json:"queued_duration"`
		RequestTime        int64           `gorm:"default:null" json:"request_time"`
		RequestDuration    float64         `gorm:"default:null" json:"request_duration"`
		ProcessingTime     int64           `gorm:"default:null" json:"processing_time"`
		ProcessingDuration float64         `gorm:"default:null" json:"processing_duration"`
		DecodingTime       int64           `gorm:"default:null" json:"decoding_time"`
		DecodingDuration   float64         `gorm:"default:null" json:"decoding_duration"`
		Duration           float64         `gorm:"default:null" json:"duration"`
		Request            httpstat.Result `gorm:"embedded" json:"request"`
	} `gorm:"embedded" json:"timings"`
	IsSearchPage   bool    `gorm:"type:tinyint;default:0" json:"is_search_page"`
	IsProductPage  bool    `gorm:"type:tinyint;default:0" json:"is_product_page"`
	IsTopLevelPage bool    `gorm:"type:tinyint;default:0" json:"is_top_level_page"`
	Elapsed        float64 `json:"elapsed"`
}

// Filter sets the information to return in the items field.
func (crawlItemsSearch *CrawlItemsSearch) Filter(uuid string) {
	// Check for valid arugment
	if crawlItemsSearch.Pagination.CurrentPage < 1 {
		crawlItemsSearch.Search.IsInvalid()
	} else {
		
		db := database.CreateDBConnection()

		crawl := Crawl{}
		crawl.SetByUUID(uuid)

		db.Where("crawl_id = ?", crawl.ID).Find(&[]CrawlItem{}).Count(&crawlItemsSearch.Results.Total)

		if crawlItemsSearch.Results.Total <= 0 {
			crawlItemsSearch.Search.IsInvalid()
		} else {
			db.
				Where("crawl_id = ?", crawl.ID).
				Order("is_processing ASC, created_at ASC").
				Offset((crawlItemsSearch.Pagination.CurrentPage - 1) * crawlItemsSearch.Query.ItemsPerPage).
				Limit(crawlItemsSearch.Query.ItemsPerPage).
				Find(&crawlItemsSearch.Items)

			crawlItemsSearch.Results.TotalReturned = len(crawlItemsSearch.Items)
			crawlItemsSearch.Search.SetPagination(envy.Get("URLSADMINCRAWLITEMS", "/"), "")
			crawlItemsSearch.Search.SetResults()
		}
		database.CloseDBConnection(db)
	}
}
