// Package models contains all models
package models

import (
	"encoding/json"
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
)

// Domain providers
var ebuyer DomainProvider

// A DomainProvider Contains the DOM selectors and Regex for a given domain.
type DomainProvider struct {
	DOMElements struct {
		Product struct {
			Price       DOMSelector `json:"price"`
			Images      DOMSelector `json:"images"`
			Seller      DOMSelector `json:"seller"`
			Description DOMSelector `json:"description"`
			Rating      DOMSelector `json:"rating"`
			Review      struct {
				Link DOMSelector `json:"link"`
				Text DOMSelector `json:"text"`
			}
			Title DOMSelector `json:"title"`
		}
		Search struct {
			TotalPages  DOMSelector `json:"total_pages"`
			CurrentPage DOMSelector `json:"current_page"`
			ProductURL  DOMSelector `json:"product_url"`
		}
		Generic struct {
			CanCrawlURLs DOMSelector `json:"can_crawl_urls"`
		}
	}
	Regex struct {
		Product struct {
			Price           string `json:"price"`
			Currency        string `json:"currency"`
			NumberOfReviews string `json:"num_of_reviews"`
			ID              string `json:"id"`
			Page            string `json:"page"`
			AssetURL        string `json:"asset_url"`
			Rating          struct {
				Actual string `json:"actual"`
				Check  string `json:"check"`
			}
		}
		Search struct {
			NumOfPages  string `json:"num_of_pages"`
			Page        string `json:"page"`
			TotalPages  string `json:"total_pages"`
			CurrentPage string `json:"current_page"`
		}
		TopLevel struct {
			Page string `json:"page"`
		}
	}
	Config struct {
		HasVariation bool
	}
}

type DomainProviders struct {
	Ebuyer DomainProvider
}

// A DOMSelector contains a selector and attribute to use in conjunction with goquery
type DOMSelector struct {
	Selector  string `json:"selector"`
	Attribute string `json:"attribute"`
}


// Load will parse a JSON file of a domain into a struct
func (domain *DomainProvider) Load(name string) {
	configFile := helpers.ReadFile("./config/domains/" + name + ".json")
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&domain)
	configFile.Close()
}

// Get sets the correct domain provider
func (domains *DomainProviders) Get(d string) DomainProvider {
	switch strings.ToLower(d) {
	case "www.ebuyer.com":
		return domains.Ebuyer
	default:
		return domains.Ebuyer
	}
}


// Load loads all domains parsers
func (domains *DomainProviders) Load() {
	ebuyer := DomainProvider{}
	ebuyer.Load("Ebuyer")
	domains.Ebuyer = ebuyer
}