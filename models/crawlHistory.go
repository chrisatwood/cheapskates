package models

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"

	"github.com/google/uuid"
)

// CrawlHistory holds all crawl items within last 2 weeks
type CrawlHistory struct {
	StartDate         time.Time          `json:"start_date"`
	EndDate           time.Time          `json:"end_date"`
	CrawlHistoryItems []CrawlHistoryItem `json:"items"`
}

// CrawlHistoryItem holds information on each crawl
type CrawlHistoryItem struct {
	UUID    uuid.UUID  `json:"uuid"`
	Date    time.Time  `json:"date"`
	Results CrawlItems `json:"results"`
}

// Get will set crawlHistory will all the data
func (crawlHistory *CrawlHistory) Get() {
	var crawls []Crawl

	db := database.CreateDBConnection()
	notFound := db.Where("is_finished = ? OR is_crawling = ? AND created_at >= ? ", 1, 1, time.Now().AddDate(0, 0, -14)).Find(&crawls).RecordNotFound()
	database.CloseDBConnection(db)

	if !notFound {
		crawlHistory.StartDate = crawls[0].CreatedAt
		crawlHistory.EndDate = crawls[len(crawls)-1].CreatedAt

		for _, crawl := range crawls {

			crawlItems := CrawlItems{
				CrawlID: crawl.ID,
			}

			crawlItems.Results()
			item := CrawlHistoryItem{
				Date:    crawl.CreatedAt,
				UUID:    crawl.UUID,
				Results: crawlItems,
			}

			crawlHistory.CrawlHistoryItems = append(crawlHistory.CrawlHistoryItems, item)
		}
	}
}
