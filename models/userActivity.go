package models

import (
	"fmt"
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"

	"github.com/jinzhu/gorm"
)

// UserActivity hold information regarding a admin activity
type UserActivity struct {
	DefaultModel
	UserID      uint
	ActionID    uint
	User        User       `gorm:"foreignkey:UserID"`
	Action      UserAction `gorm:"foreignkey:ActionID"`
	ActionValue string     `gorm:"type:text"`
	RemoteIP    string     `gorm:"type:varchar(45)"`
	UserAgent   string     `gorm:"type: text"`
}

// Migrate sets all foreign keys
func (userActivity UserActivity) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&userActivity)
	database.CloseDBConnection(db)
}

// TableName changes UserActivity table name
func (UserActivity) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("ADMINPREFIX", "") + envy.Get("USERSPREFIX", "") + envy.Get("USERACTIVITIES", "")
}

//AfterCreate adds uuid to user activity record
func (userActivity *UserActivity) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(userActivity).Update("uuid", uuid.New())
	return
}

// Create will create an action by name
func (userActivity *UserActivity) Create(actionName string) {
	action := UserAction{
		Name: actionName,
	}

	if !action.Exists() {
		action.Create()
	}

	userActivity.Action = action
}

// Update will update the record in the Database
func (userActivity *UserActivity) Update() {
	db := database.CreateDBConnection()
	db.Save(&userActivity)
	database.CloseDBConnection(db)
}

// Get will create a new DB itm
func (userActivity *UserActivity) Get(uaID uint) {
	db := database.CreateDBConnection()
	f := db.Where("id = ?", uaID).First(&userActivity).RecordNotFound()
	fmt.Println(uaID)

	fmt.Println("f", f)
	database.CloseDBConnection(db)
}
