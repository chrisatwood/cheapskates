package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// A Product contains the name of the product, the price, the images,
// the seller, description and the product rating.
type Product struct {
	DefaultModel
	Name            string             `gorm:"index:name"`
	Description     string             `gorm:"type:text"`
	Variations      []ProductVariation `gorm:"foreignkey:ProductID"`
	PriceHistory    []ProductPrice     `gorm:"foreignkey:ProductID"`
	Images          []ProductImage     `gorm:"foreignkey:ProductID"`
	Sellers         []ProductSeller    `gorm:"foreignkey:ProductID"`
}

// Migrate sets all foreign keys
func (product Product) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&product)
	database.CloseDBConnection(db)
}

// TableName changes Products table name
func (Product) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTS", "")
}

//AfterCreate adds uuid to product record
func (product *Product) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(product).Update("uuid", uuid.New())
	return
}

// Exists will return if the product already exists
func (product *Product) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("name = ?", product.Name).First(&product).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (product *Product) Create() {
	db := database.CreateDBConnection()
	db.Create(&product)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (product *Product) Update() {
	db := database.CreateDBConnection()
	db.Save(&product)
	database.CloseDBConnection(db)
}

//SetByUUID returns product by productUUID
func (product *Product) SetByUUID(productUUID string) bool {
	db := database.CreateDBConnection()
	notFound := db.Where("uuid = ?", productUUID).Preload("Images").Preload("PriceHistory").First(&product).RecordNotFound()
	database.CloseDBConnection(db)
	return !notFound
}
