package models

import (
	"fmt"
	"net/url"
	"sync"
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// Crawl Model that holds all information for a given crawl.
type Crawl struct {
	DefaultModel
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	Elapsed   float64   `gorm:"default:null" json:"elapsed"`
	Status    struct {
		IsCrawling  bool `gorm:"type:tinyint;default:0" json:"is_crawling"`
		IsFinished  bool `gorm:"type:tinyint;default:0" json:"is_finished"`
		IsManual    bool `gorm:"type:tinyint;default:0" json:"is_manual"`
		IsCancelled bool `gorm:"type:tinyint;default:0" json:"is_cancelled"`
	} `json:"status" gorm:"embedded"`
	CrawlItems []CrawlItem `gorm:"foreignkey:CrawlID" json:"-"`
	Results    struct {
		Total          uint `json:"total"`
		TotalCompleted uint `json:"total_completed"`
		TotalFailed    uint `json:"total_failed"`
	} `json:"results" gorm:"embedded"`
	ActioningUserID uint `gorm:"default:null"  json:"-"`
	ActioningUser   User `gorm:"foreignkey:ActioningUserID;preload:false" json:"-"`
	Settings        struct {
		BaseURLs        []CrawlBaseURL `gorm:"type:text" json:"base_urls"`
		Limit           uint           `json:"limit"`
		ConcurrentDelay uint           `json:"concurrent_delay"`
		Units           uint           `json:"units"`
	} `json:"settings" gorm:"embedded"`
}

// wg is the waitgroup for the application.
var wg sync.WaitGroup

// proxies holds all proxies
var proxies Proxies

// domains holds all domain providers
var domains DomainProviders

// UAs holds alla User agents
var UAs UserAgents

// isTerminated checks if finished has been called
var isTerminated bool

// Migrate creates foreign keys for crawl item
func (crawl Crawl) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&crawl)
	db.Model(&crawl).AddForeignKey("actioning_user_id", database.GetDBTableNameAndFeild([]string{"ADMINPREFIX", "USERS"}, "id"), "RESTRICT", "RESTRICT")
	database.CloseDBConnection(db)
}

// TableName changes Crawls table name
func (Crawl) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("CRAWLS", "")
}

// AfterCreate Adds uuid to crawl record
func (crawl *Crawl) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(crawl).Update("uuid", uuid.New())
	return
}

// Update will update the DB
func (crawl *Crawl) Update() {
	db := database.CreateDBConnection()
	db.Save(&crawl)
	database.CloseDBConnection(db)
}

// Init will initialise functions
func (crawl *Crawl) Init() {
	startTime := time.Now()

	// Load all domain providers
	domains = DomainProviders{}
	proxies = Proxies{}
	UAs = UserAgents{}

	domains.Load()
	proxies.Load()
	UAs.Load()

	// Update crawl
	crawl.StartTime = startTime
	crawl.EndTime = startTime
	crawl.Status.IsCrawling = true

	isTerminated = false

	crawl.Create()
	fmt.Println("Log: Crawl created. ", crawl.ID)
}

// SetBaseURLs creates a crawlItem for each base URL given
func (crawl *Crawl) SetBaseURLs() {
	for _, item := range crawl.Settings.BaseURLs {
		// Initate the root url
		crawlItem := CrawlItem{
			CrawlID: crawl.ID,
			URL:     item.URL,
			IsRoot:  true,
		}

		u, _ := url.Parse(item.URL)

		if !crawlItem.Exists() {
			parser := Parser{
				URL:    u,
				Parser: domains.Get(u.Host),
			}

			crawlItem.Init(parser.GetPageType(u))
		}
	}
}

// SetUser sets the crawl with the given user by the given UUID.
// @Param uuidSTR The UUID of the user
func (crawl *Crawl) SetUser(uuidSTR string) bool {
	user := User{}

	user.UUID, _ = uuid.Parse(uuidSTR)

	if user.Exists() {
		crawl.ActioningUser = user
		crawl.Update()
		return true
	}
	return false
}

// Begin will start the crawler
func (crawl *Crawl) Begin() {
	crawlItems := CrawlItems{
		CrawlID: crawl.ID,
		Crawl:   crawl,
	}
	crawlItems.GetNext()

	if crawlItems.TotalQueued > 0 {
		time.Sleep(time.Duration(crawl.Settings.ConcurrentDelay) * time.Millisecond)

		crawlItems.Items[0].Process(false)

		for index := 0; uint(index) < crawl.Settings.Units; index++ {
			wg.Add(1)
			crawl.AddOne(0)
		}
	} else {
		if !isTerminated {
			isTerminated = true
			crawl.Finish()
		}
	}
}

// AddOne will schedule next items to be processed
func (crawl *Crawl) AddOne(counter int) {
	crawlItems := CrawlItems{
		CrawlID: crawl.ID,
		Crawl:   crawl,
	}

	crawlItems.GetNextOne()

	if crawlItems.TotalQueued > 0 {
		time.Sleep(time.Duration(crawl.Settings.ConcurrentDelay) * time.Millisecond)
		wg.Add(1)

		go crawlItems.Items[0].Process(true)
	} else {
		counter++
		if counter <= helpers.ParseInt(envy.Get("CRAWLATTEMPTLIMIT", "3")) {
			crawl.AddOne(counter)
		} else {
			if !isTerminated {
				isTerminated = true
				crawl.Finish()
			}
		}
	}
}

// Finish will terminate the crawler and set performance metrics
func (crawl *Crawl) Finish() {
	// Get last finished item for the correct duration
	crawlItem := CrawlItem{
		CrawlID: crawl.ID,
	}

	isFound := crawlItem.Last()

	if isFound {
		fmt.Println(crawlItem.Timings.ProcessingTime, (time.Duration(crawlItem.Timings.ProcessingDuration) * time.Second))
		crawl.EndTime = time.Unix(0, crawlItem.Timings.ProcessingTime).Add(time.Duration(crawlItem.Timings.ProcessingDuration) * time.Second)
		crawl.Elapsed = crawl.EndTime.Sub(crawl.StartTime).Seconds()
	} else {
		crawl.EndTime = time.Now()
		crawl.Elapsed = time.Since(crawl.StartTime).Seconds()
	}

	if !crawl.Status.IsCancelled {
		crawl.Status.IsFinished = true
	}
	crawl.Status.IsCrawling = false

	// Add total number of pages crawled
	crawlItems := CrawlItems{
		CrawlID: crawl.ID,
	}
	crawlItems.Results()

	crawl.Results.Total = crawlItems.TotalQueued
	crawl.Results.TotalFailed = crawlItems.TotalFailed
	crawl.Results.TotalCompleted = crawlItems.TotalCompleted

	crawl.Update()
}

// LimitReached returns bool if max number of pages were crawled.
func (crawl *Crawl) LimitReached() bool {
	crawlItems := CrawlItems{
		CrawlID: crawl.ID,
	}
	crawlItems.Count()
	return crawlItems.TotalQueued >= crawl.Settings.Limit
}

// ProcessPageURLs adds new crawl items
func (crawl *Crawl) ProcessPageURLs(URLs []*url.URL) {
	totalAdded := 0
	for _, u := range URLs {
		if uint(totalAdded+1) < crawl.Settings.Limit {
			if !crawl.LimitReached() {
				crawlItem := CrawlItem{
					URL:     u.String(),
					CrawlID: crawl.ID,
					IsRoot:  false,
				}

				canBeAdded := !crawlItem.Exists()
				if canBeAdded {
					totalAdded++
					parser := Parser{
						URL:    u,
						Parser: domains.Get(u.Host),
					}

					crawlItem.Init(parser.GetPageType(u))
				}
			}
		}
	}
}

// Create creates a DB record
func (crawl *Crawl) Create() {
	db := database.CreateDBConnection()
	db.Create(&crawl)
	database.CloseDBConnection(db)
}

// Set will set the crawl item
func (crawl *Crawl) Set(crawlID uint) {
	db := database.CreateDBConnection()
	db.First(&crawl, crawlID)
	database.CloseDBConnection(db)
}

// SetByUUID will set the crawl item by UUID
func (crawl *Crawl) SetByUUID(crawlUUID string) bool {
	db := database.CreateDBConnection()
	notFound := db.Where("uuid = ?", crawlUUID).First(&crawl).RecordNotFound()
	database.CloseDBConnection(db)
	return !notFound
}

// Cancel will terminate a crawl.
func (crawl *Crawl) Cancel() {
	var crawlItems []CrawlItem

	db := database.CreateDBConnection()
	db.Where("crawl_id = ? AND is_complete = ? && is_processing = ?", crawl.ID, 0, 0).Find(&crawlItems)
	database.CloseDBConnection(db)

	for _, crawlItem := range crawlItems {
		crawlItem.AddError("Crawl item cancelled", "cancelled", "crawl has been cancelled.")
	}
	crawl.Status.IsCancelled = true
	crawl.Update()
	crawl.Finish()
}

// HasFailed checks if a given crawl has failed
func (crawl Crawl) HasFailed() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("elapsed IS NULL").First(&crawl).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}
