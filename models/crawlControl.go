package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
)

// CrawlControlsActive checks if a crawl is currently crawling and the crawl itself
type CrawlControlsActive struct {
	Status struct {
		IsCrawling bool `json:"crawling"`
	} `json:"status"`
	Item Crawl `json:"item"`
}

// Get will set the latest crawl which is crawling
func (crawlControlsActive *CrawlControlsActive) Get() {
	db := database.CreateDBConnection()
	notFound := db.Where("is_crawling >= ?", 1).First(&crawlControlsActive.Item).RecordNotFound()
	database.CloseDBConnection(db)

	crawlControlsActive.Status.IsCrawling = !notFound 
}
