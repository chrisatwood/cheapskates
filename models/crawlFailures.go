package models

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
)

// CrawlFailures holds stats about number of failures
type CrawlFailures struct {
	Total      int  `json:"total" default:"0"`
	IsPositive bool `json:"positive"`
	IsNegative bool `json:"negative"`
	IsStable   bool `json:"stable"`
	Difference struct {
		Percentage float32 `json:"percentage" default:"0"`
		Value      int     `json:"value" default:"0"`
	} `json:"difference"`
}

// Get sets the number of failures including percentages
func (crawlFailures *CrawlFailures) Get() {
	var currentWeekcrawls []Crawl
	var lastWeekcrawls []Crawl
	lastWeekTotal := 0
	db := database.CreateDBConnection()

	notFound := db.Where("created_at >= ?", time.Now().AddDate(0, 0, -7)).Find(&currentWeekcrawls).RecordNotFound()

	if !notFound {
		for _, crawl := range currentWeekcrawls {
			crawlFailures.Total += int(crawl.Results.TotalFailed)
		}
	}

	notFound = db.Where("created_at BETWEEN ? AND ?", time.Now().AddDate(0, 0, -14), time.Now().AddDate(0, 0, -7)).Find(&lastWeekcrawls).RecordNotFound()

	database.CloseDBConnection(db)
	if !notFound {
		for _, crawl := range lastWeekcrawls {
			lastWeekTotal += int(crawl.Results.TotalFailed)
		}
	}

	crawlFailures.Difference.Percentage = float32((float64(lastWeekTotal) / float64(crawlFailures.Total)) * 100)

	diff := (lastWeekTotal - crawlFailures.Total)

	crawlFailures.IsNegative = diff < 0
	crawlFailures.IsStable = diff == 0
	crawlFailures.IsPositive = diff > 0

	if diff < 0 {
		diff = -diff
	}
	crawlFailures.Difference.Value = diff
}
