package models

import (
	"io"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"github.com/PuerkitoBio/goquery"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
	httpstat "github.com/tcnksm/go-httpstat"
)

// CrawlItem contains information about a given page crawled.
type CrawlItem struct {
	DefaultModel
	CrawlID   uint      `gorm:"default:null" json:"crawl_id"`
	TimeAdded time.Time `gorm:"default: current_timestamp" json:"time_added"`
	Timings   struct {
		QueuedTime                 int64           `gorm:"default:null" json:"queued_time"`
		Queued                     float64         `gorm:"default:null" json:"queued_duration"`
		RequestTime                int64           `gorm:"default:null" json:"request_time"`
		RequestDuration            float64         `gorm:"default:null" json:"request_duration"`
		ContentTransfer            float64         `gorm:"default:null" json:"content_transfer_duration"`
		RequestPreperationDuration float64         `gorm:"default:null" json:"request_preperation_duration"`
		ProcessingTime             int64           `gorm:"default:null" json:"processing_time"`
		ProcessingDuration         float64         `gorm:"default:null" json:"processing_duration"`
		DecodingTime               int64           `gorm:"default:null" json:"decoding_time"`
		DecodingDuration           float64         `gorm:"default:null" json:"decoding_duration"`
		Duration                   float64         `gorm:"default:null" json:"duration"`
		Request                    httpstat.Result `gorm:"embedded" json:"request"`
	} `gorm:"embedded" json:"timings"`
	URL       string `gorm:"type:text" json:"url"`
	ProductID uint   `gorm:"default:null" json:"-"`
	SkippedID uint   `gorm:"default:null" json:"-"`
	ProxyID   uint   `gorm:"default:null" json:"-"`
	Status    struct {
		IsQueued     bool `gorm:"type:tinyint;default:1" json:"is_queued"`
		IsProcessing bool `gorm:"type:tinyint;default:0" json:"is_processing"`
		IsComplete   bool `gorm:"type:tinyint;default:0" json:"is_complete"`
	} `gorm:"embedded" json:"status"`
	IsSearchPage   bool             `gorm:"type:tinyint;default:0" json:"is_search_page"`
	IsProductPage  bool             `gorm:"type:tinyint;default:0" json:"is_product_page"`
	IsTopLevelPage bool             `gorm:"type:tinyint;default:0" json:"is_top_level_page"`
	IsRoot         bool             `gorm:"type:tinyint;default:0" json:"is_roor"`
	IsSkipped      bool             `gorm:"type:tinyint;defaulf:0" json:"is_skipped"`
	SkippedReason  CrawlItemSkipped `gorm:"foreignkey:SkippedID;preload:false" json:"skipped_reason"`
	Product        Product          `gorm:"foreignkey:ProductID;preload:false" json:"product"`
	Proxy          Proxy            `gorm:"foreignkey:ProxyID;preload:false" json:"proxy"`
}

// Migrate sets all foreign keys
func (crawlItem CrawlItem) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&crawlItem)
	db.Model(&crawlItem).AddForeignKey("crawl_id", database.GetDBTableNameAndFeild([]string{"CRAWLS"}, "id"), "RESTRICT", "RESTRICT")
	db.Model(&crawlItem).AddForeignKey("product_id", database.GetDBTableNameAndFeild([]string{"PRODUCTS"}, "id"), "RESTRICT", "RESTRICT")
	db.Model(&crawlItem).AddForeignKey("skipped_id", database.GetDBTableNameAndFeild([]string{"CRAWLSPREFIX", "CRAWLITEMSPREFIX", "CRAWLITEMSKIPPED"}, "id"), "RESTRICT", "RESTRICT")
	db.Model(&crawlItem).AddForeignKey("proxy_id", database.GetDBTableNameAndFeild([]string{"PROXIES"}, "id"), "RESTRICT", "RESTRICT")

	database.CloseDBConnection(db)
}

// TableName changes CrawlItems table name
func (crawlItem CrawlItem) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("CRAWLSPREFIX", "") + envy.Get("CRAWLITEMS", "")
}

// AfterCreate Adds uuid to crawl item record
func (crawlItem *CrawlItem) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(crawlItem).Update("uuid", uuid.New())
	return
}

// Last gets crawl item that last finished
func (crawlItem *CrawlItem) Last() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("crawl_id = ? AND processing_duration > ? AND processing_time > ?", crawlItem.CrawlID, 0, 0).Order("updated_at DESC").First(&crawlItem).RecordNotFound()
	database.CloseDBConnection(db)
	return !notFound
}

// Init will preprocess a crawl item before processing
func (crawlItem *CrawlItem) Init(pageType string) {
	crawlItem.IsProductPage = (pageType == "product")
	crawlItem.IsSearchPage = (pageType == "search")
	crawlItem.IsTopLevelPage = (pageType == "top-level")
	crawlItem.Status.IsQueued = true

	if pageType == "no-match" {
		crawlItem.Status.IsQueued = false
		crawlItem.IsSkipped = true

		crawlItemSkipped := CrawlItemSkipped{
			Slug:        envy.Get("ERRORINVALIDPAGETYPE", ""),
			Description: "",
		}

		if !crawlItemSkipped.Exists() {
			crawlItemSkipped.Create()
		}
		crawlItem.SkippedReason = crawlItemSkipped
	} else {
		crawlItem.Timings.QueuedTime = time.Now().UnixNano()
	}
	crawlItem.Create()
}

// Exists will return if the product already exists
func (crawlItem *CrawlItem) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("crawl_id = ? AND url = ?", crawlItem.CrawlID, crawlItem.URL).First(&crawlItem).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new CrawlItem in the DB
func (crawlItem *CrawlItem) Create() {
	crawlItem.TimeAdded = time.Now()

	db := database.CreateDBConnection()
	db.Create(&crawlItem)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (crawlItem *CrawlItem) Update() {
	db := database.CreateDBConnection()
	db.Save(&crawlItem)
	database.CloseDBConnection(db)
}

// Process will process a crawl item
func (crawlItem *CrawlItem) Process(isContinuous bool) {
	crawlItem.Timings.Queued = time.Since(time.Unix(0, crawlItem.Timings.QueuedTime)).Seconds()
	crawlItem.Status.IsQueued = false
	crawlItem.Status.IsProcessing = true
	crawlItem.Update()

	crawl := Crawl{}
	crawl.Set(crawlItem.CrawlID)

	// Request
	response, parser, err := crawlItem.GetResponse()

	if err == nil && response != nil {
		crawlItem.Timings.DecodingTime = time.Now().UnixNano()
		doc, err := goquery.NewDocumentFromReader(response)

		crawlItem.Timings.DecodingDuration = time.Since(time.Unix(0, crawlItem.Timings.DecodingTime)).Seconds()

		response.Close()
		contentTrans := crawlItem.Timings.Request.ContentTransfer(time.Now())
		crawlItem.Timings.ContentTransfer = contentTrans.Seconds()

		requestDur := crawlItem.Timings.Request.Total(time.Now())
		crawlItem.Timings.RequestDuration = requestDur.Seconds()
		crawlItem.Timings.ProcessingTime = time.Now().UnixNano()
		if err == nil {

			crawlItem.Update()
			parser.Document = doc
			parser.CrawlItem = crawlItem
			parser.Crawl = &crawl

			if crawlItem.IsProductPage {
				parser.ParseProduct()

			} else if crawlItem.IsSearchPage {
				parser.ParseSearch()

			} else if crawlItem.IsTopLevelPage {
				parser.ParseToplevel()
			} else {
				// Not in list
				crawlItem.AddError("Invalid page type: ", envy.Get("ERRORINVALIDPAGETYPE", ""), "")
			}
		} else {
			//	HTML decoding problem

			crawlItem.Timings.ProcessingTime = time.Now().UnixNano()
			crawlItem.AddError("HTML decoding: ", envy.Get("ERRORHTMLDECODING", ""), "")
		}

		crawlItem.Timings.ProcessingDuration = time.Since(time.Unix(0, crawlItem.Timings.ProcessingTime)).Seconds()
		crawlItem.Timings.Duration = time.Since(time.Unix(0, crawlItem.Timings.RequestTime)).Seconds()
		crawlItem.Update()
	}

	if isContinuous {
		wg.Done()
		crawl.AddOne(0)
	}
}

// AddError will add an error to the crawl item and insert new skipped
// reason into the DB if new.
func (crawlItem *CrawlItem) AddError(prefix string, slug string, description string) {

	crawl := Crawl{}
	crawl.Set(crawlItem.CrawlID)

	crawlItem.Status.IsQueued = false
	crawlItem.IsSkipped = true
	crawlItem.Status.IsProcessing = false
	crawlItem.Status.IsComplete = true

	crawlItemSkipped := CrawlItemSkipped{
		Slug:        slug,
		Description: description,
	}

	if !crawlItemSkipped.Exists() {
		crawlItemSkipped.Create()
	}
	crawlItem.SkippedReason = crawlItemSkipped
	crawlItem.Update()
}

// AddProxy will create a new proxy item int eh DB if needed and
// Add reference to
func (crawlItem *CrawlItem) AddProxy(proxy Proxy) {
	if !proxy.Exists() {
		proxy.Create()
	}

	crawlItem.Proxy = proxy
	crawlItem.Update()
}

// GetResponse will set a proxy and user agent and get the response from a given URL
func (crawlItem *CrawlItem) GetResponse() (io.ReadCloser, Parser, error) {
	crawlItem.Timings.RequestTime = time.Now().UnixNano()
	proxy := proxies.Random()
	crawlItem.AddProxy(proxy)

	req, err := http.NewRequest("GET", crawlItem.URL, nil)

	// Set proxy
	transport := &http.Transport{
		Proxy: http.ProxyURL(proxy.Url()),
	}

	client := &http.Client{
		Transport: transport,
	}
	req.Header.Set("User-Agent", UAs.Random().UA)

	ctx := httpstat.WithHTTPStat(req.Context(), &crawlItem.Timings.Request)
	req = req.WithContext(ctx)

	crawlItem.Timings.RequestPreperationDuration = time.Since(time.Unix(0, crawlItem.Timings.RequestTime)).Seconds()
	res, err := client.Do(req)

	if err != nil {
		crawlItem.AddError("GET request: : ", envy.Get("ERRORPAGEREQUESTERROR", ""), err.Error())
		return nil, Parser{}, err
	}

	if res.StatusCode != 200 {
		crawlItem.AddError("GET request: ", envy.Get("ERRORBADREQUESTSTATUS", ""), res.Status+" - "+crawlItem.URL)
		return nil, Parser{}, err
	}

	u, _ := url.Parse(crawlItem.URL)
	parser := Parser{
		URL:    u,
		Parser: domains.Get(u.Host),
	}

	return res.Body, parser, nil
}

// GetTestResponse will look at local files instead of http requests
func (crawlItem *CrawlItem) GetTestResponse() (io.ReadCloser, Parser, []error) {
	u, _ := url.Parse(crawlItem.URL)
	parser := Parser{
		URL:    u,
		Parser: domains.Get(u.Host),
	}

	pageType := parser.GetPageType(u)
	html := helpers.ReadFile("./test/pages/" + pageType + ".html")

	return html, parser, nil
}
