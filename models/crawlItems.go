package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
)

// CrawlItems holds stats and current crawl items
type CrawlItems struct {
	TotalQueued     uint         `default:"0" json:"total_queued"`
	TotalProcessing uint         `default:"0" json:"total_processing"`
	TotalFailed     uint         `default:"0" json:"total_failed"`
	TotalCompleted  uint         `default:"0" json:"total_completed"`
	TotalProducts   uint         `default:"0" json:"total_product_pages"`
	TotalSearches   uint         `default:"0" json:"total_search_pages"`
	TotalTopLevels  uint         `default:"0" json:"total_top_level_pages"`
	CrawlID         uint         `json:"-"`
	Crawl           *Crawl       `json:"-"`
	Items           []*CrawlItem `json:"-"`
}

// GetNextOne will set the next 1 item
func (crawlItems *CrawlItems) GetNextOne() {
	db := database.CreateDBConnection()
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_processing = ?", crawlItems.CrawlID, 1).Count(&crawlItems.TotalProcessing)

	limit := uint(1)

	if crawlItems.TotalProcessing < crawlItems.Crawl.Settings.Units {
		db.Where("crawl_id = ? AND is_queued = ?", crawlItems.CrawlID, 1).Order("time_added ASC").Limit(limit).Find(&crawlItems.Items)

		crawlItems.TotalQueued = uint(len(crawlItems.Items))
	}
	database.CloseDBConnection(db)
}

// GetNext will set the next n items
func (crawlItems *CrawlItems) GetNext() {
	db := database.CreateDBConnection()
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_processing = ?", crawlItems.CrawlID, 1).Count(&crawlItems.TotalProcessing)

	limit := crawlItems.Crawl.Settings.Units
	limit = limit - crawlItems.TotalProcessing

	db.Where("crawl_id = ? AND is_queued = ?", crawlItems.CrawlID, 1).Order("time_added ASC").Limit(limit).Find(&crawlItems.Items)

	crawlItems.TotalQueued = uint(len(crawlItems.Items))
	database.CloseDBConnection(db)
}

// Count Sets crawlItems that has been process by the crawler
func (crawlItems *CrawlItems) Count() {
	db := database.CreateDBConnection()
	db.Model(&CrawlItem{}).Where("crawl_id = ?", crawlItems.CrawlID).Count(&crawlItems.TotalQueued)
	database.CloseDBConnection(db)
}

// Results will set the results of a crawl.
func (crawlItems *CrawlItems) Results() {
	db := database.CreateDBConnection()
	db.Model(&CrawlItem{}).Where("crawl_id = ?", crawlItems.CrawlID).Count(&crawlItems.TotalQueued)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_skipped = ?", crawlItems.CrawlID, 1).Count(&crawlItems.TotalFailed)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_complete = ? AND is_skipped = ?", crawlItems.CrawlID, 1, 0).Count(&crawlItems.TotalCompleted)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_complete = ? AND is_product_page = ?", crawlItems.CrawlID, 1, 1).Count(&crawlItems.TotalProducts)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_complete = ? AND is_product_page = ?", crawlItems.CrawlID, 1, 1).Count(&crawlItems.TotalProducts)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_complete = ? AND is_search_page = ?", crawlItems.CrawlID, 1, 1).Count(&crawlItems.TotalSearches)
	db.Model(&CrawlItem{}).Where("crawl_id = ? AND is_complete = ? AND is_top_level_page = ?", crawlItems.CrawlID, 1, 1).Count(&crawlItems.TotalTopLevels)

	database.CloseDBConnection(db)
}

// GetAllByProductID Return all products by ID
func (crawlItems *CrawlItems) GetAllByProductID(id uint) {
	db := database.CreateDBConnection()
	db.Where("product_id = ?", id).Group("url").Find(&crawlItems.Items)
	database.CloseDBConnection(db)
}
