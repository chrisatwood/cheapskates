package models

import (
	"net/url"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
)

// ProductSearch extends Search and returns all search items
type ProductSearch struct {
	Search
	Items []ProductSearchItem
}

type ProductSearchItem struct {
	Product
	URL string `json:"url"`
}

// Filter sets the information to return in the items field.
func (productSearch *ProductSearch) Filter(url *url.URL) {
	db := database.CreateDBConnection()

	// Check for valid arugment
	if productSearch.Pagination.CurrentPage < 1 {
		productSearch.Search.IsInvalid()
	} else {
		db.Where("name LIKE ?", "%"+productSearch.Query.Query+"%").Find(&[]Product{}).Count(&productSearch.Results.Total)

		if productSearch.Results.Total <= 0 {
			productSearch.Search.IsInvalid()
		} else {
			db.
				Order("name DESC").
				Where("name LIKE ?", "%"+productSearch.Query.Query+"%").
				Offset((productSearch.Pagination.CurrentPage - 1) * productSearch.Query.ItemsPerPage).
				Preload("Sellers").
				Preload("Variations").
				Preload("PriceHistory").
				Preload("Images").
				Limit(productSearch.Query.ItemsPerPage).
				Find(&productSearch.Items)

			for i := range productSearch.Items {
				productSearch.Items[i].URL = envy.Get("URLSPRODUCTVIEW", "/") + productSearch.Items[i].UUID.String()
			}

			productSearch.Results.TotalReturned = len(productSearch.Items)
			productSearch.Search.SetPagination(envy.Get("URLSPRODUCTS", "/"), url.RawQuery)
			productSearch.Search.SetResults()
		}
	}
	database.CloseDBConnection(db)
}
