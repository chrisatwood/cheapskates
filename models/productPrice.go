package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// The ProductPrice contains the raw price and the currency the product.
type ProductPrice struct {
	DefaultModel
	ProductID     uint   `gorm:"default:null" json:"-"`
	URL           string `gorm:"type:text"`
	Price         float32
	DeliveryPrice float32
	CurrencyCode  string  `gorm:"type:varchar(10)"`
	Product       Product `gorm:"foreignkey:ProductID" json:"-"`
}

// Migrate sets all foreign keys
func (price ProductPrice) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&price)

	db.Model(&price).AddForeignKey("product_id", database.GetDBTableNameAndFeild([]string{"PRODUCTS"}, "id"), "RESTRICT", "RESTRICT")
	database.CloseDBConnection(db)
}

// TableName changes ProductPrices table name
func (ProductPrice) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTSPREFIX", "") + envy.Get("PRODUCTPRICES", "")
}

// AfterCreate add uuid to  ProductPrices record
func (price *ProductPrice) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(price).Update("uuid", uuid.New())
	return
}

// Create will create a new DB item
func (price *ProductPrice) Create() {
	db := database.CreateDBConnection()
	db.Create(&price)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (price *ProductPrice) Update() {
	db := database.CreateDBConnection()
	db.Save(&price)
	database.CloseDBConnection(db)
}
