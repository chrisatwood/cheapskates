package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// CrawlBaseURL holds information for a base url for a crawl
type CrawlBaseURL struct {
	DefaultModel
	CrawlID uint   `gorm:"default:null"  json:"-"`
	Crawl   Crawl  `gorm:"foreignkey:CrawlID;preload:false" json:"-"`
	URL     string `json:"url"`
}

// Migrate handles migrations
func (crawlBaseURL CrawlBaseURL) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&crawlBaseURL)
	db.Model(&crawlBaseURL).AddForeignKey("crawl_id", database.GetDBTableNameAndFeild([]string{"CRAWLS"}, "id"), "RESTRICT", "RESTRICT")

	database.CloseDBConnection(db)
}

// TableName changes Crawls table name
func (CrawlBaseURL) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("CRAWLSPREFIX", "") + envy.Get("CRAWLBASEURL", "")
}

// AfterCreate Adds uuid to crawl record
func (crawlBaseURL *CrawlBaseURL) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(&crawlBaseURL).Update("uuid", uuid.New())
	return
}

// Update will update the DB
func (crawlBaseURL *CrawlBaseURL) Update() {
	db := database.CreateDBConnection()
	db.Save(&crawlBaseURL)
	database.CloseDBConnection(db)
}

// Create creates a DB record
func (crawlBaseURL *CrawlBaseURL) Create() {
	db := database.CreateDBConnection()
	db.Create(&crawlBaseURL)
	database.CloseDBConnection(db)
}
