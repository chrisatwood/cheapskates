package models

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// The ProductImage contains the source/ href value of the image.
type ProductImage struct {
	DefaultModel
	ProductID      uint    `gorm:"default:null"`
	Source         string  `json:"src"`
	OriginalSource string  `json:"original_source"`
	Product        Product `gorm:"foreignkey:ProductID"`
}

// Migrate sets all foreign keys
func (image ProductImage) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&image)
	db.Model(&image).AddForeignKey("product_id", database.GetDBTableNameAndFeild([]string{"PRODUCTS"}, "id"), "RESTRICT", "RESTRICT")
	database.CloseDBConnection(db)
}

// TableName changes ProductImages table name
func (ProductImage) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PRODUCTSPREFIX", "") + envy.Get("PRODUCTIMAGES", "")
}

// AfterCreate add uuid to  ProductImages record
func (image *ProductImage) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(image).Update("uuid", uuid.New())
	return
}

// Exists will return if the image already exists
func (image ProductImage) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("(original_source = ? || source = ?) && product_id = ?", image.OriginalSource, image.Source, image.Product.ID).First(&image).RecordNotFound()
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (image *ProductImage) Create() {
	db := database.CreateDBConnection()
	db.Create(&image)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (image *ProductImage) Update() {
	db := database.CreateDBConnection()
	db.Save(&image)
	database.CloseDBConnection(db)
}

// Upload will upload image to cloudinary
func (image *ProductImage) Upload(path string) error {
	response, _ := http.PostForm(envy.Get("CLOUDINARY_UPLOAD_URL", "")+envy.Get("CLOUDINARY_NAME", "")+"/image/upload", url.Values{
		"file":          {image.OriginalSource},
		"upload_preset": {envy.Get("CLOUDINARY_UPLOAD_PRESET", "")},
		"folder":        {path},
	})

	if response.StatusCode == 400 {
		var cerror CloudinaryError

		body, _ := ioutil.ReadAll(response.Body)
		err := json.Unmarshal(body, &cerror)

		if err != nil {
			return errors.New("Could not decode error")
		}

		return errors.New(cerror.Error.Message)
	}

	var cresponse CloudinaryResponse

	body, _ := ioutil.ReadAll(response.Body)
	err := json.Unmarshal(body, &cresponse)

	if err != nil {
		return err
	}

	image.Source = cresponse.SecureURL
	image.Update()
	return nil
}
