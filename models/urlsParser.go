package models

import (
	"net/url"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"github.com/PuerkitoBio/goquery"
)

// GetPageURLs return all URLs on a page
func (parser *Parser) GetPageURLs() {
	parser.Document.Find(parser.Parser.DOMElements.Generic.CanCrawlURLs.Selector).
		Each(func(i int, s *goquery.Selection) {
			var URL *url.URL

			if parser.Parser.DOMElements.Generic.CanCrawlURLs.Attribute != "null" {
				attr, _ := s.Attr(parser.Parser.DOMElements.Generic.CanCrawlURLs.Attribute)
				URL, _ = url.Parse(attr)

			} else {
				URL, _ = url.Parse(s.Text())
			}

			//Get Page type
			pageType := parser.GetPageType(URL)

			// Only check if it's a product or a search page
			if pageType == "product" || pageType == "search" {
				URL = parser.AddDomainToRelativeURL(URL)
				parser.Pages = append(parser.Pages, URL)
			}
		})
}

// GetPageType return the page type of a URL
func (parser Parser) GetPageType(url *url.URL) string {
	isProduct := helpers.RegexIsMatching(url.String(), parser.Parser.Regex.Product.Page)
	isSearch := helpers.RegexIsMatching(url.String(), parser.Parser.Regex.Search.Page)
	isTopLevel := helpers.RegexIsMatching(url.String(), parser.Parser.Regex.TopLevel.Page)

	if isProduct {
		// Product page
		return "product"
	} else if isSearch {
		// search page
		return "search"
	} else if isTopLevel {
		// Top-level page
		return "top-level"
	} else {
		return "no-match"
	}
}

// SetProductPages will add a crawl item for each product on a page
func (parser *Parser) SetProductPages() {
	parser.Document.Find(parser.Parser.DOMElements.Search.ProductURL.Selector).Each(func(i int, s *goquery.Selection) {
		var u *url.URL
		if parser.Parser.DOMElements.Search.ProductURL.Attribute != "null" {
			attr, _ := s.Attr(parser.Parser.DOMElements.Search.ProductURL.Attribute)
			u, _ = url.Parse(attr)
		} else {
			u, _ = url.Parse(s.Text())
		}

		parser.Pages = append(parser.Pages, parser.AddDomainToRelativeURL(u))
	})
}

// SetSearchPages will add crawlItems for each crawl page
// between the current page and the total num of pages.
func (parser *Parser) SetSearchPages() {
	for i := parser.CurrentPage + 1; i <= parser.TotalPages; i++ {
		query := parser.URL.Query()

		query.Set("page", string(i))
		u := parser.AddDomainToRelativeURL(parser.URL)

		parser.Pages = append(parser.Pages, u)
	}
}

// AddDomainToRelativeURL adds a scheme and host to a url object
func (parser Parser) AddDomainToRelativeURL(url *url.URL) *url.URL {
	url.Host = parser.URL.Host
	url.Scheme = "https"
	return url
}
