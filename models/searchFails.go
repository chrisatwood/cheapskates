package models

import (
	"bitbucket.org/chrisatwood/cheapskates/database"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
)

// CrawlSearch extends Search and returns all search items
type CrawlFailureSearch struct {
	Search
	UUID  uuid.UUID   `json:"uuid"`
	Items []CrawlItem `json:"items"`
}

// Filter sets the information to return in the items field.
func (crawlFailureSearch *CrawlFailureSearch) Filter(uuid string) {
	db := database.CreateDBConnection()

	// Check for valid arugment
	if crawlFailureSearch.Pagination.CurrentPage < 1 {
		crawlFailureSearch.Search.IsInvalid()
	} else {
		crawl := Crawl{}
		crawl.SetByUUID(uuid)

		crawlFailureSearch.UUID = crawl.UUID

		db.Where("crawl_id = ? and is_skipped = ?", crawl.ID, 1).Find(&[]CrawlItem{}).Count(&crawlFailureSearch.Results.Total)

		if crawlFailureSearch.Results.Total <= 0 {
			crawlFailureSearch.Search.IsInvalid()
		} else {
			db.
				Where("crawl_id = ? and is_skipped = ?", crawl.ID, 1).
				Order("created_at DESC").
				Offset((crawlFailureSearch.Pagination.CurrentPage - 1) * crawlFailureSearch.Query.ItemsPerPage).
				Preload("SkippedReason").
				Preload("Proxy").
				Limit(crawlFailureSearch.Query.ItemsPerPage).
				Find(&crawlFailureSearch.Items)

			crawlFailureSearch.Results.TotalReturned = len(crawlFailureSearch.Items)
			crawlFailureSearch.Search.SetPagination(envy.Get("URLSADMINCRAWLFAILS", "/"), "")
			crawlFailureSearch.Search.SetResults()
		}
	}
	database.CloseDBConnection(db)
}
