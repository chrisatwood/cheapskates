package models

type CloudinaryResponse struct {
	PublicID         string   `json:"public_id"`
	Version          int32    `json:"version"`
	Signature        string   `json:"signature"`
	Width            int32    `json:"width"`
	Height           int32    `json:"height"`
	Format           string   `json:"format"`
	CreatedAt        string   `json:"created_at"`
	Tags             []string `json:"tags"`
	Bytes            int32    `json:"bytes"`
	Type             string   `json:"type"`
	ETag             string   `json:"etag"`
	Placeholder      bool     `json:"placeholder"`
	URL              string   `json:"url"`
	SecureURL        string   `json:"secure_url"`
	Existing         bool     `json:"existing"`
	OriginalFilename string   `json:"original_filename"`
}

type CloudinaryError struct {
	Error CloudinaryErrorMessage `json:"error"`
}

type CloudinaryErrorMessage struct {
	Message string `json:"message"`
}


