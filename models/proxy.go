package models

import (
	"encoding/json"
	"net/url"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// Proxy is information for a given proxy
type Proxy struct {
	DefaultModel
	URL    string `json:"url"`
	Port   string `json:"port"`
	Scheme string `json:"scheme"`
}

// Proxies hold list of Proxy
type Proxies struct {
	Total   int8    `json:"total"`
	Proxies []Proxy `json:"proxies"`
}

// Migrate sets all foreign keys
func (proxy Proxy) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&proxy)
	database.CloseDBConnection(db)
}

// TableName changes ProductRatings table name
func (Proxy) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("PROXIES", "")
}

//AfterCreate adds uuid to product record
func (proxy *Proxy) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(proxy).Update("uuid", uuid.New())
	return
}

// String Converts a proxy to string
func (proxy Proxy) String() string {
	return proxy.Scheme + "://" + proxy.URL + ":" + string(proxy.Port)
}

// Url Converts a proxy to a URL
func (proxy Proxy) Url() *url.URL {
	u, _ := url.Parse(proxy.Scheme + "://" + proxy.URL + ":" + string(proxy.Port))
	return u
}

// Exists will return if the rating already exists
func (proxy *Proxy) Exists() bool {
	db := database.CreateDBConnection()
	notFound := db.Where("port = ? AND url = ? AND scheme = ?", proxy.Port, proxy.URL, proxy.Scheme).Find(&proxy).RecordNotFound()

	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (proxy *Proxy) Create() {
	db := database.CreateDBConnection()
	db.Create(&proxy)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (proxy *Proxy) Update() {
	db := database.CreateDBConnection()
	db.Save(&proxy)
	database.CloseDBConnection(db)
}

// Load will load proxies from a json file
func (proxies *Proxies) Load() {
	proxyFile := helpers.ReadFile("./config/proxies.json")

	jsonParser := json.NewDecoder(proxyFile)
	jsonParser.Decode(&proxies)
	proxyFile.Close()
}

// Random will select a random proxy
func (proxies Proxies) Random() Proxy {
	return proxies.Proxies[helpers.GetRandomInt(int(proxies.Total)-1)]
}
