package models

import (
	"html"
	"net/url"
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"

	"github.com/PuerkitoBio/goquery"
)

// Parser contains info about processing a URL
type Parser struct {
	Document    *goquery.Document
	Parser      DomainProvider
	URL         *url.URL
	Crawl       *Crawl
	CrawlItem   *CrawlItem
	Product     Product
	Rating      ProductRating
	TotalPages  int8
	CurrentPage int8
	Pages       []*url.URL
}

// ParseProduct parses a product
func (parser *Parser) ParseProduct() {
	parser.Product = Product{}
	parser.CreateProduct()
	parser.CreateProductPrice()
	parser.SetProductRating()
	parser.SetProductSeller()
	parser.SetProductImages()
	parser.Product.Update()
	parser.GetPageURLs()

	// Process all urls on this page
	parser.Crawl.ProcessPageURLs(parser.Pages)

	parser.CrawlItem.ProductID = parser.Product.ID
	parser.CrawlItem.Status.IsProcessing = false
	parser.CrawlItem.Status.IsComplete = true

	parser.CrawlItem.Update()
}

// CreateProduct creates a product record
func (parser *Parser) CreateProduct() {
	parser.SetProductName()
	parser.SetProductDescription()
	if parser.Product.Exists() {
		parser.Product.Update()
	} else {
		parser.Product.Create()
	}
}

// SetProductDescription Sets the product description
func (parser *Parser) SetProductDescription() {
	descHTML, _ := parser.Document.Find(parser.Parser.DOMElements.Product.Description.Selector).Html()
	parser.Product.Description = html.EscapeString(helpers.RemoveImages(helpers.RemoveTabsAndLines(descHTML)))
}

// SetProductName Sets the product name
func (parser *Parser) SetProductName() {
	title := parser.Document.Find(parser.Parser.DOMElements.Product.Title.Selector)

	if parser.Parser.DOMElements.Product.Title.Attribute != "" {
		attrs, _ := title.Attr(parser.Parser.DOMElements.Product.Title.Attribute)

		parser.Product.Name = attrs
	} else {
		parser.Product.Name = title.Text()
	}
}

// CreateProductPrice creates a product price record
func (parser *Parser) CreateProductPrice() {
	var price string
	priceRaw := parser.Document.Find(parser.Parser.DOMElements.Product.Price.Selector)

	if parser.Parser.DOMElements.Product.Price.Attribute != "null" {
		price, _ = priceRaw.Attr(parser.Parser.DOMElements.Product.Price.Attribute)
	} else {
		price = strings.TrimSpace(priceRaw.Text())
	}

	productPrice := ProductPrice{}

	productPrice.Product = parser.Product
	productPrice.URL = parser.URL.String()
	productPrice.Price = helpers.ParseFloat(helpers.RegexReplaceSubString(price, parser.Parser.Regex.Product.Price, ""))
	productPrice.CurrencyCode = helpers.RegexReplaceSubString(price, parser.Parser.Regex.Product.Currency, "")
	productPrice.Create()
}

// SetProductImages Sets all images for a given product
func (parser Parser) SetProductImages() {
	var images []ProductImage

	parser.Document.Find(parser.Parser.DOMElements.Product.Images.Selector).Each(func(i int, s *goquery.Selection) {
		if parser.Parser.DOMElements.Product.Images.Attribute != "null" {
			src, _ := s.Attr("data-src")
			if helpers.RegexIsMatching(src, parser.Parser.Regex.Product.AssetURL) {
				images = append(images, ProductImage{
					OriginalSource: src,
					Source:         src,
					Product:        parser.Product,
				})
			}
		} else {
			if helpers.RegexIsMatching(s.Text(), parser.Parser.Regex.Product.AssetURL) {
				images = append(images, ProductImage{
					OriginalSource: s.Text(),
					Source:         s.Text(),
					Product:        parser.Product,
				})
			}
		}
	})

	for _, image := range images {
		// Check if original course exists
		if !image.Exists() {
			image.Create()
			domaindPID := strings.Replace(strings.Replace(helpers.RegexGetMatching(parser.URL.String(), parser.Parser.Regex.Product.ID), "-", "", -1), "/", "", -1)

			_ = image.Upload(parser.URL.Host + "/" + domaindPID)
		}
	}
}

// SetProductSeller will create a product seller record
func (parser *Parser) SetProductSeller() {
	seller := ProductSeller{}

	if parser.Parser.DOMElements.Product.Seller.Selector != "" {
		raw := parser.Document.Find(parser.Parser.DOMElements.Product.Seller.Selector)
		URL, _ := raw.Attr(parser.Parser.DOMElements.Product.Seller.Attribute)

		if !strings.Contains(URL, parser.URL.Host) {
			URL = parser.URL.Host + URL
		}
		seller.Name = raw.Text()
		seller.URL = URL

	} else {
		seller.URL = parser.URL.Host
		seller.Name = parser.URL.Host
	}

	seller.Rating = parser.Rating
	seller.Product = parser.Product

	if seller.Exists() {
		seller.Update()
	} else {
		seller.Create()
	}
}

// SetProductRating will create a product rating record
func (parser *Parser) SetProductRating() {
	parser.SetRatingStats()

	if parser.Parser.DOMElements.Product.Rating.Selector != "" {
		numRaw := parser.Document.Find(parser.Parser.DOMElements.Product.Review.Text.Selector).Text()

		total := helpers.GetNumOfReviews(parser.Parser.Regex.Product.NumberOfReviews, numRaw)
		link := parser.Document.Find(parser.Parser.DOMElements.Product.Review.Link.Selector)

		href, _ := link.Attr("href")

		if strings.Index(href, "#") == 0 {
			href = parser.URL.String() + href
		}

		parser.Rating.NumberOfReviews = int32(total)
		parser.Rating.URL = href

	} else if parser.Parser.DOMElements.Product.Review.Link.Selector != "" {
		if string(parser.Parser.DOMElements.Product.Review.Link.Selector[0]) == "#" {

			parser.Rating.URL = parser.URL.String() + parser.Parser.DOMElements.Product.Review.Link.Selector
			parser.Rating.NumberOfReviews = 0
		}
	}

	if parser.Rating.Exists() {
		parser.Rating.Update()
	} else {
		parser.Rating.Create()
	}
}

// SetRatingStats sets the max and the current rating of a product
func (parser *Parser) SetRatingStats() {
	var ratingF float32
	var max int16

	ratingRaw := parser.Document.Find(parser.Parser.DOMElements.Product.Rating.Selector)
	titleRaw, _ := ratingRaw.Attr(parser.Parser.DOMElements.Product.Rating.Attribute)

	switch parser.URL.Host {
	case "www.amazon.com":
		ratingS := helpers.RegexReplaceSubString(titleRaw, parser.Parser.Regex.Product.Rating.Actual, "")

		ratingF = helpers.ParseFloat(ratingS)
		max = 5
	default:
		ratingF, max = float32(0.0), 5
	}

	parser.Rating.MaxStars = max
	parser.Rating.Stars = ratingF
}
