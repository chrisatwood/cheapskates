package models

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/database"
	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"golang.org/x/crypto/bcrypt"

	"github.com/gobuffalo/envy"
	uuid "github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// User holds all information regarding a admin
type User struct {
	DefaultModel
	Email      string `gorm:"UNIQUE"`
	Forename   string `gorm:"type:varchar(50)"`
	Surname    string `gorm:"type:varchar(50)"`
	Hash       string `json:"_"`
	HashExpiry time.Time
	Activities []UserActivity `gorm:"foreignkey:UserID"`
}

// Migrate sets all foreign keys
func (user User) Migrate() {
	db := database.CreateDBConnection()
	db.AutoMigrate(&user)
	database.CloseDBConnection(db)
}

// TableName changes User table name
func (User) TableName() string {
	return envy.Get("DBPREFIX", "") + envy.Get("ADMINPREFIX", "") + envy.Get("USERS", "")
}

//AfterCreate adds uuid to user record
func (user *User) AfterCreate(scope *gorm.Scope) (err error) {
	scope.DB().Model(user).Update("uuid", uuid.New())
	return
}

// GenerateHash will has a user password
func (user *User) GenerateHash(password string) bool {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), helpers.ParseInt(envy.Get("BCRYPTCOST", "10")))
	if err != nil {
		return false
	}

	user.Hash = string(bytes)
	user.HashExpiry = time.Now().AddDate(0, 3, 0)

	user.Create()
	return true
}

// CheckAuthenication will check a hash password by the string
func (user User) CheckAuthenication(password string) bool {
	return (bcrypt.CompareHashAndPassword([]byte(user.Hash), []byte(password))) == nil
}

// Exists will return if the product already exists
func (user *User) Exists() bool {
	db := database.CreateDBConnection()

	var notFound bool
	if user.Email == "" && user.UUID.String() != "" {
		notFound = db.Where("uuid = ?", user.UUID).First(&user).RecordNotFound()
	} else {
		notFound = db.Where("email = ?", user.Email).First(&user).RecordNotFound()

	}
	database.CloseDBConnection(db)

	return !notFound
}

// Create will create a new DB item
func (user *User) Create() {
	db := database.CreateDBConnection()
	db.Create(&user)
	database.CloseDBConnection(db)
}

// Update will update the record in the Database
func (user *User) Update() {
	db := database.CreateDBConnection()
	db.Save(&user)
	database.CloseDBConnection(db)
}

// Get will create a new DB itm
func (user *User) Get(userID uint) {
	db := database.CreateDBConnection()
	db.Where("id = ?", userID).First(&user)
	database.CloseDBConnection(db)
}

// Logout will terminate a session and cookie
func (user User) Logout(hash string) {
	if len(hash) != 0 {
		session := Session{
			Hash: hash,
		}

		if session.Exists() {
			session.Delete()
		}
	}
}
