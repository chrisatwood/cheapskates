package helpers

import (
	"testing"

	"github.com/gobuffalo/envy"
)

func TestRegexCompile(t *testing.T) {
	envy.Load("../.env.test")
	regex := RegexCompile(envy.Get("TESTREGEX", ""))

	if regex == nil {
		t.Errorf("RegexCompile failed. Expected value *regexp.Regexp, instead got nil")
	}
}

func TestRegexReplaceSubString(t *testing.T) {
	envy.Load("../.env.test")
	s := RegexReplaceSubString("failed", envy.Get("TESTREGEX", "/"), "passed")

	if s != "passed" {
		t.Errorf("RegexReplaceSubString failed. Expected value %s, instead got %s", "passed", s)
	}
}

func TestRegexGetMatching(t *testing.T) {
	str := RegexGetMatching("a", envy.Get("TESTREGEX", ""))

	if str != "a" {
		t.Errorf("RegexGetMatching failed. Expected value %s, instead got %s", "a", str)
	}
}

func TestRegexIsMatching(t *testing.T) {
	r := RegexIsMatching("127.01", envy.Get("TESTREGEX", ""))

	if r != false {
		t.Errorf("RegexIsMatching failed. Expected value %t, instead got %t", false, r)
	}
}
