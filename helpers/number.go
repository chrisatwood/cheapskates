package helpers

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

// GetRandomInt Returns random int
func GetRandomInt(max int) int {
	rand.Seed(time.Now().UnixNano())
	if max > 0 {
		return rand.Intn(max)
	}
	return 0
}

// ParseInt coverts a string to a int format
func ParseInt(str string) int {
	i, _ := strconv.ParseInt(str, 10, 0)
	return int(i)
}

// ParseInt8 coverts a string to a int8 format
func ParseInt8(str string) int8 {
	i, _ := strconv.ParseInt(str, 10, 8)
	return int8(i)
}

// ParseFloat Converts a string into a float.
func ParseFloat(str string) float32 {
	if str != "" {
		s, err := strconv.ParseFloat(str, 32)
		if err != nil {
			fmt.Println(err)
		}
		return float32(s)
	}
	return float32(0)
}
