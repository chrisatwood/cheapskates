package helpers

import (
	"bytes"
	"encoding/gob"
)

// InterfaceToBytes converts interface to bytes
func InterfaceToBytes(v interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(v)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
