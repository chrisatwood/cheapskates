package helpers

import (
	"os"
)

// ReadFile read file
func ReadFile(path string) *os.File {
	f, err := os.Open(path)

	if err != nil {
		return nil
	}
	return f
}
