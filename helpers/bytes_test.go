package helpers

import "testing"

func TestInterfaceToBytes(t *testing.T) {
	type TestInterface struct {
		Name string
	}

	ti := TestInterface{Name: "Christopher"}

	_, err := InterfaceToBytes(ti)

	if err != nil {
		t.Errorf("TestInterface failed decode interface. Want %s, instead got %s", "[]byte", err.Error())
	}
}
