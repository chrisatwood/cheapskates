package helpers

import "testing"

func TestGetCSS(t *testing.T) {
	css := GetCSSTest("404")

	if len(css) == 0 {
		t.Errorf("GetCSS Failed. Expected length > %d instead got %d", 0, len(css))
	}
}
