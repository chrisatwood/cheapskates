package helpers

import (
	"github.com/gin-gonic/gin"
)

// HasCookie checks if cooke exists
func HasCookie(c *gin.Context, name string) bool {
	_, err := c.Cookie(name)
	return err == nil
}
