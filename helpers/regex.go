//Package helpers contains regex helpers
package helpers

import (
	"fmt"
	"regexp"
)

// RegexCompile complies the regex string
func RegexCompile(regex string) *regexp.Regexp {
	reg, err := regexp.Compile(regex)
	if err != nil {
		fmt.Println("Error compile regex: ", err)
		return nil
	}
	return reg
}

// RegexReplaceSubString replace a the matching regex with the given string
func RegexReplaceSubString(text string, regex string, replace string) string {
	reg := RegexCompile(regex)
	if reg != nil {
		return reg.ReplaceAllString(text, replace)
	}
	return text
}

//RegexGetMatching returns the matching part of the string
func RegexGetMatching(text string, regex string) string {
	reg := RegexCompile(regex)
	if reg != nil {
		return reg.FindString(text)
	}
	return text
}

// RegexIsMatching returns whether a give string matches a regex.
// Takes 2 string params
// Returns a boolean
func RegexIsMatching(text string, regex string) bool {
	reg, err := regexp.Compile(regex)
	if err != nil {
		return false
	}

	return reg.MatchString(text)
}
