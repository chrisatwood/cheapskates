package helpers

import (
	"strconv"
	"strings"
)

// IsEmpty checks if a string is empty
func IsEmpty(data string) bool {
	return len(data) <= 0
}

// RemoveTabsAndLines removes tabs from string
func RemoveTabsAndLines(str string) string {
	return strings.TrimSpace(RegexReplaceSubString(str, "([\t\n])+", ""))
}

// GetNumOfReviews returns number of reviews
func GetNumOfReviews(regex string, text string) int {
	return ParseInt(RegexReplaceSubString(text, regex, ""))
}

//ParseStringBool converts bool to string
func ParseStringBool(value bool) string {
	return strconv.FormatBool(value)
}

//ParseStringUint convers uint to string
func ParseStringUint(value uint) string {
	return strconv.FormatUint(uint64(value), 10)
}

// RemoveImages removes image tags from description
func RemoveImages(str string) string {
	return RegexReplaceSubString(str, "/<img[^>]+>/gi", "")
}