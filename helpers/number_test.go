package helpers

import (
	"testing"
)

func TestGetRandomInt(t *testing.T) {
	randomInt := GetRandomInt(10)

	if randomInt < 0 || randomInt > 10 {
		t.Errorf("RandomInt failed. Expected value between %d and %d, instead got %d", 0, 10, randomInt)
	}
}

// ParseInt coverts a string to a int format
func TestParseInt(t *testing.T) {
	i := ParseInt("127")

	if i != int(127) {
		t.Errorf("ParseInt failed. Expected value %d, instead got %d", 127, i)
	}
}

// ParseInt8 coverts a string to a int8 format
func TestParseInt8(t *testing.T) {
	i := ParseInt8("127")

	if i != int8(127) {
		t.Errorf("ParseInt failed. Expected value %d, instead got %d", 127, i)
	}
}

// ParseFloat Converts a string into a float.
func TestParseFloat(t *testing.T) {
	f := ParseFloat("127.01")

	if f != float32(127.01) {
		t.Errorf("ParseInt failed. Expected value %f, instead got %f", 127.01, f)
	}
}
