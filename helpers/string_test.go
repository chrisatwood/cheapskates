package helpers

import (
	"testing"
)

func TestIsEmpty(t *testing.T) {
	b := IsEmpty(" ")

	if b == true {
		t.Errorf("IsEmpty failed. Expected value %t, instead got %t", false, b)
	}
}

func TestRemoveTabsAndLines(t *testing.T) {
	str := RemoveTabsAndLines("		")

	if str != "" {
		t.Errorf("RemoveTabsAndLines failed. Expected value string length %d, instead got %d", 0, len(str))
	}
}

func TestParseStringBool(t *testing.T) {
	b := ParseStringBool(false)
	if b != "false" {
		t.Errorf("ParseStringBool failed. Expected value %s, instead got %s", "false", b)
	}
}

func TestParseStringUint(t *testing.T) {
	num := ParseStringUint(uint(127))
	if num != "127" {
		t.Errorf("ParseStringUint failed. Expected value %s, instead got %s", "127", num)
	}
}
