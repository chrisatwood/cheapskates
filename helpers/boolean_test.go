package helpers

import (
	"testing"
)

func TestParseBool(t *testing.T) {
	b := ParseBool("true")

	if b == false {
		t.Errorf("ParseBool failed parse. Want %v, instead got %v", true, b)
	}

	b = ParseBool("false") 

	if b == true {
		t.Errorf("ParseBool failed parse. Want %v, instead got %v", false, b)
	}
}