package helpers

import "testing"

func TestReadFile(t *testing.T) {
	f := ReadFile("../test/pages/top-level.html")

	if f == nil {
		t.Errorf("ReadFile Failed. Expected length file type, instead got nil")

	}
}
