package helpers

import (
	"bufio"
	"io"
	"os"
)

// ReadTestFile reutrns a sample HTML document
func ReadTestFile(pageType string) io.Reader {
	f, _ := os.Open("test-"+ pageType+".html")

	return bufio.NewReader(f)
}
