package helpers

import "strconv"

// ParseBool parses a str into bool
func ParseBool(str string) bool {
	b, err := strconv.ParseBool(str)
	if err != nil {
		return false
	}
	return b
}
