package helpers

import (
	"html/template"
	"io/ioutil"
)

// GetCSS returns style CSS for above the fold rendering.
func GetCSS(filename string) template.HTML {
	file, err := ioutil.ReadFile("./assets/dist/css/above-fold/" + filename + ".css")

	if err != nil {
		return ""
	}
	return NoEscape("<style>" + string(file) + "</style>")
}

func GetCSSTest(filename string) template.HTML {
	file, err := ioutil.ReadFile("../assets/dist/css/above-fold/" + filename + ".css")

	if err != nil {
		return ""
	}
	return NoEscape("<style>" + string(file) + "</style>")
}
// NoEscape Converts string into HTML
func NoEscape(str string) template.HTML {
	return template.HTML(str)
}
