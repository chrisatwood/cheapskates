// Package controllers contains all controller functions for routing for the API and the web site
package controllers

import (
	"net/http"
	"strings"

	"github.com/google/uuid"

	"bitbucket.org/chrisatwood/cheapskates/models"

	"bitbucket.org/chrisatwood/cheapskates/helpers"

	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
)

// CreateUser creates a new development user
func CreateUser(c *gin.Context) {
	user := models.User{
		Forename: "Chris",
		Surname:  "Atwood",
		Email:    "chrisatwood50@gmail.com",
	}

	password := envy.Get("ADMINPASSWORD", "password")
	user.GenerateHash(password)

	userActivity := models.UserActivity{
		ActionValue: "Create creds",
	}

	userActivity.Create("created-hash")

	userActivity.RemoteIP = helpers.GetIPAdress(c.Request)
	userActivity.UserAgent = c.Request.Header.Get("User-Agent")

	userActivity.Update()

}

// AdminLogin is a controller for the ANy page that does not match a route
func AdminLogin(c *gin.Context) {
	emailErr := helpers.ParseBool(c.Query("ee"))
	passErr := helpers.ParseBool(c.Query("pe"))
	globalErr := helpers.ParseBool(c.Query("ge"))
	timeoutErr := helpers.ParseBool(c.Query("toe"))

	gintemplate.HTML(c, http.StatusOK, "auth/login", gin.H{
		"title":              "Cheapskates - Administration - Login",
		"subtitle":           "Administration login",
		"lead":               "In order to view this page, you must be logged in.",
		"bodyClass":          "hero",
		"baseCSS":            helpers.GetCSS("auth/login"),
		"emailErr":           emailErr,
		"passErr":            passErr,
		"globalErr":          globalErr,
		"timeoutErr":         timeoutErr,
		"emailErrMessage":    "Please enter your email address.",
		"passwordErrMessage": "Please enter your password.",
		"globalErrMessage":   "Your credentials do not match.",
		"expiredErrMessage":  "Your session has expired. Please sign in.",
		"Scripts":            []string{"/assets/js/login.js"},
	})
}

// AdminLoginPost handles login attempts
func AdminLoginPost(c *gin.Context) {
	var user models.User

	emailErr := false
	passErr := false
	globalErr := false
	timeoutErr := false

	sessionValue, err := c.Cookie(envy.Get("COOKIENAME", "cookie"))
	url := envy.Get("URL", "")

	if err == nil && sessionValue != "" {
		session := models.Session{
			Hash: sessionValue,
		}

		if !session.Exists() {
			userID := session.UserActivity.User.ID
			session.Delete()
			user = models.User{}

			user.Get(userID)

			value := uuid.New().String()

			ua := models.UserActivity{
				User:        user,
				ActionValue: "login requested",
			}

			ua.Create("login")

			session := models.Session{
				UserActivity: ua,
				Hash:         value,
			}

			session.Create()

			c.SetCookie(envy.Get("COOKIENAME", "cookie"), value, helpers.ParseInt(envy.Get("COOKIEEXPIRY", "1800")), "/", strings.Replace(url, "https://", "", -1), true, false)
			c.Redirect(302, "/admin/")
			c.Abort()
		} else {
			c.SetCookie(envy.Get("COOKIENAME", "cookie"), "", -1, "/", strings.Replace(url, "https://", "", -1), true, false)
			timeoutErr = true
		}
	} else {
		email, emailFound := c.GetPostForm("email")
		password, passwordFound := c.GetPostForm("password")

		if !emailFound {
			emailErr = true
		}

		if !passwordFound {
			passErr = true
		}

		user.Email = email

		if user.Exists() {
			if user.CheckAuthenication(password) {
				value := uuid.New().String()

				ua := models.UserActivity{
					User:        user,
					ActionValue: "login requested",
				}

				ua.Create("login")

				session := models.Session{
					UserActivity: ua,
					Hash:         value,
				}

				session.Create()
				c.SetCookie(envy.Get("COOKIENAME", "cookie"), value, helpers.ParseInt(envy.Get("COOKIEEXPIRY", "1800")), "/", strings.Replace(url, "https://", "", -1), true, false)
			} else {
				globalErr = true
			}
		} else {
			globalErr = true
		}
	}
	if emailErr || passErr || globalErr || timeoutErr {
		c.Redirect(http.StatusSeeOther, "/auth/login?ee="+helpers.ParseStringBool(emailErr)+"&pe="+helpers.ParseStringBool(passErr)+"&ge="+helpers.ParseStringBool(globalErr)+"&toe="+helpers.ParseStringBool(timeoutErr))
		c.Abort()
	} else {
		c.Redirect(http.StatusSeeOther, "/admin/")
		c.Abort()
	}
}
