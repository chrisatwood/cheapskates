// Package controllers contains all controller functions for routing for the API and the web site
package controllers

import (
	"net/http"
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"

	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
)

// PageNotFoundController is a controller for the ANy page that does not match a route
func PageNotFoundController(c *gin.Context) {
	if strings.HasPrefix(c.Request.URL.Path, "/api/") {
		c.Status(http.StatusNotFound)
		c.Abort()
	} else {
		gintemplate.HTML(c, http.StatusOK, "pages/pageNotFound", gin.H{
			"title":    "Cheapskates - Page not found",
			"subtitle": "Opps , something went wrong!",
			"lead":     "The page that you were looking for does not exists.",
			"baseCSS":  helpers.GetCSS("404"),
			"bodyCSS":  "",
		})
	}
}
