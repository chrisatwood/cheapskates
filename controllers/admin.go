package controllers

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/chrisatwood/cheapskates/models"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
)


// AdminIndex is a controller for the homepage
func AdminIndex(c *gin.Context) {
	userName, _ := c.Get("userName")
	userUUID, _ := c.Get("userUUID")

	gintemplate.HTML(c, http.StatusOK, "admin/index", gin.H{
		"UserName": userName,
		"UserUUID": userUUID,
		"title":    "Cheapskates - Administration homepage",
		"baseCSS":  helpers.GetCSS("admin/index"),
		"IsAdmin":  true,
		"Scripts":  []string{"/assets/projects/crawl-graph/js/manifest.js", "/assets/projects/crawl-graph/js/vendor.js", "/assets/projects/crawl-graph/js/app.js"},
	})
}

// AdminIndex is a controller for the homepage
func AdminCrawls(c *gin.Context) {
	userName, _ := c.Get("userName")
	userUUID, _ := c.Get("userUUID")

	crawlSearch := models.CrawlSearch{}

	crawlSearch.Query.ItemsPerPage = helpers.ParseInt(c.Param("total"))
	crawlSearch.Pagination.CurrentPage = helpers.ParseInt(c.Param("page"))

	if crawlSearch.Query.ItemsPerPage == 0 {
		crawlSearch.Query.ItemsPerPage = helpers.ParseInt(envy.Get("TOTALPERPAGE", "6"))
	}

	if crawlSearch.Pagination.CurrentPage == 0 {
		crawlSearch.Pagination.CurrentPage = 1
	}

	crawlSearch.Filter()

	gintemplate.HTML(c, http.StatusOK, "admin/crawls", gin.H{
		"UserName":   userName,
		"UserUUID":   userUUID,
		"title":      "Cheapskates - Crawls",
		"baseCSS":    helpers.GetCSS("admin/index"),
		"IsAdmin":    true,
		"DateFormat": envy.Get("DATEFORMAT", ""),
		"Search":     crawlSearch,
	})
}

func AdminCrawlView(c *gin.Context) {
	userName, _ := c.Get("userName")
	userUUID, _ := c.Get("userUUID")

	crawl := models.Crawl{}

	crawl.SetByUUID(c.Param("uuid"))

	crawlItems := models.CrawlItems{
		CrawlID: crawl.ID,
	}

	crawlItems.Results()

	if crawl.Elapsed == 0 {
		crawl.Elapsed = time.Since(crawl.StartTime).Seconds()
	}

	gintemplate.HTML(c, http.StatusOK, "admin/crawl", gin.H{
		"UserName":   userName,
		"UserUUID":   userUUID,
		"title":      "Cheapskates - Crawls",
		"baseCSS":    helpers.GetCSS("admin/index"),
		"IsAdmin":    true,
		"Crawl":      crawl,
		"DateFormat": envy.Get("DATEFORMAT", ""),
		"CrawlItems": crawlItems,
		"Scripts":    []string{"https://unpkg.com/popper.js@1/dist/umd/popper.min.js", "https://unpkg.com/tippy.js@4", "/assets/js/crawlPage.js", "/assets/projects/crawl-items/js/manifest.js", "/assets/projects/crawl-items/js/vendor.js", "/assets/projects/crawl-items/js/app.js"},
		"FormatFloat": func(f float64) float64 {
			return float64(int(f*10000)) / 10000
		},
		"Percentage": func(i int) float64 {
			return (float64(i) / float64(crawlItems.TotalCompleted+crawlItems.TotalFailed)) * 100
		},
		"FormatDuration": func(f float64) string {
			dur := time.Duration(time.Duration(f) * time.Second).String()
			if dur == "0" {
				return "Crawling"
			}
			return dur
		},
	})

}

func AdminLogout(c *gin.Context) {
	url := envy.Get("URL", "")

	userRaw, _ := c.Get("user")
	cookieRaw, _ := c.Get("cookie")
	var user models.User
	var cookie string

	userB, err1 := helpers.InterfaceToBytes(userRaw)
	cookieB, err2 := helpers.InterfaceToBytes(cookieRaw)

	json.Unmarshal(userB, &user)
	json.Unmarshal(cookieB, &cookie)

	if err1 != nil || err2 != nil {

		c.SetCookie(envy.Get("COOKIENAME", "cookie"), "", 0, "/", strings.Replace(url, "https://", "", -1), false, false)
		c.Redirect(http.StatusMovedPermanently, envy.Get("URLSHOME", "/"))
		c.Abort()
	} else {

		user.Get(user.ID)
		ua := models.UserActivity{
			User:        user,
			ActionValue: "logout requested",
		}

		ua.Create("logout")
		user.Logout(cookie)
		c.SetCookie(envy.Get("COOKIENAME", "cookie"), "", 0, "/", strings.Replace(url, "https://", "", -1), false, false)

		c.Redirect(http.StatusMovedPermanently, envy.Get("URLSHOME", "/"))
		c.Abort()
	}

}
