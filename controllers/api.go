package controllers

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"bitbucket.org/chrisatwood/cheapskates/models"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
)

// APICrawlResults API controller for crawl results
func APICrawlResults(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	ch := models.CrawlHistory{}
	ch.Get()

	c.JSON(http.StatusOK, ch)
}

// APICrawlStatus API controller for crawl status
func APICrawlStatus(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	cs := models.CrawlStatus{}
	cs.Get()

	c.JSON(http.StatusOK, cs)
}

// APICrawlFailures API controller for crawl failures
func APICrawlFailures(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	cs := models.CrawlFailures{}
	//cs.Get()

	c.JSON(http.StatusOK, cs)
}

// APICrawlControls API controller for crawl controls
func APICrawlControls(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	cc := models.CrawlControlsActive{}

	cc.Get()
	c.JSON(http.StatusOK, cc)
}

// APICrawlCancel API controller for cancelling a crawl
func APICrawlCancel(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	crawl := models.Crawl{}
	if crawl.SetByUUID(c.Param("uuid")) {
		crawl.Cancel()
	} else {
		c.Status(http.StatusBadRequest)
	}
	c.Status(http.StatusOK)
	c.Abort()
}

type InvalidRequest struct {
	Error      string   `json:"error"`
	FailedURLs []string `json:"failed_urls"`
}

// APICrawlStart is for initating the web scraping crawl
func APICrawlStart(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")

	user := c.Query("auuid")
	if len(user) == 0 {
		c.AbortWithStatus(http.StatusForbidden)
	} else {
		var hasError bool
		urls := strings.Split(c.Query("urls"), ";")

		if len(urls) <= 0 {
			urls = strings.Split(envy.Get("CRAWLSTARTURLS", "https://www.ebuyer.com/"), ";")
		}

		crawl := models.Crawl{}
		crawl.Settings.Limit = uint(helpers.ParseInt(c.Query("limit")))
		crawl.Settings.ConcurrentDelay = uint(helpers.ParseInt(c.Query("delay")))
		crawl.Settings.Units = uint(helpers.ParseInt(c.Query("units")))

		if crawl.Settings.Limit == 0 {
			crawl.Settings.Limit = uint(helpers.ParseInt(envy.Get("CRAWLITEMSLIMIT", "122")))
		}

		// Checking delay
		if crawl.Settings.ConcurrentDelay != 0 &&
			crawl.Settings.ConcurrentDelay < uint(helpers.ParseInt(envy.Get("CRAWLMINDELAY", "250"))) ||
			crawl.Settings.ConcurrentDelay > uint(helpers.ParseInt(envy.Get("CRAWLMAXDELAY", "2250"))) {
			c.JSON(403, InvalidRequest{
				Error: "Invalid concurrentDelay",
			})
			hasError = true
			c.Abort()
		} else if crawl.Settings.ConcurrentDelay == 0 {
			crawl.Settings.ConcurrentDelay = uint(helpers.ParseInt(envy.Get("CRAWLDELAY", "250")))
		}

		if crawl.Settings.Units != 0 &&
			crawl.Settings.Units < uint(helpers.ParseInt(envy.Get("CRAWLMINUNITS", "1"))) ||
			crawl.Settings.Units > uint(helpers.ParseInt(envy.Get("CRAWLMAXUNITS", "5"))) {
			c.JSON(403, InvalidRequest{
				Error: "Invalid Units",
			})

			hasError = true
			c.Abort()
		} else if crawl.Settings.Units == 0 {
			crawl.Settings.Units = uint(helpers.ParseInt(envy.Get("CRAWLUNITS", "3")))
		}

		if !hasError {
			crawl.Init()
			if crawl.SetUser(user) {
				failedURLs := []string{}

				for _, urlR := range urls {

					if len(urlR) > 0 && urlR[len(urlR)-1] == '/' {
						urlR = urlR[:len(urlR)-1]
					}
					u, err := url.Parse(urlR)
					
					fmt.Println("Log: Adding Base url - ", urlR)

					if (strings.Contains(envy.Get("CRAWLSTARTURLS", "https://www.ebuyer.com/"), u.Host) && err == nil) && u.Host != "" {
						fmt.Print(" - OKAY")
						crawlURL := models.CrawlBaseURL{
							Crawl: crawl,
							URL:   urlR,
						}
						crawlURL.Create()
						crawl.Settings.BaseURLs = append(crawl.Settings.BaseURLs, crawlURL)

					} else {
						fmt.Print(" - Failed")
						failedURLs = append(failedURLs, urlR)
					}
				}

				if len(failedURLs) == 0 {
					crawl.SetBaseURLs()
					go crawl.Begin()

					c.JSON(200, crawl)
				} else {

					ir := InvalidRequest{
						Error:      "Invalid start URLs",
						FailedURLs: failedURLs,
					}
					crawl.Status.IsCancelled = true
					crawl.Status.IsCrawling = false
					crawl.Status.IsFinished = true
					crawl.Update()
					c.JSON(400, ir)
				}
			} else {
				c.AbortWithStatus(http.StatusForbidden)
			}
		}
	}
}

// APICrawlFailReasons API controller for fail reasons
func APICrawlFailReasons(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	cr := models.CrawlFailureSearch{}

	cr.Query.ItemsPerPage = helpers.ParseInt(c.Param("total"))
	cr.Pagination.CurrentPage = helpers.ParseInt(c.Param("page"))

	if cr.Query.ItemsPerPage == 0 {
		cr.Query.ItemsPerPage = helpers.ParseInt(envy.Get("TOTALPERPAGE", "6"))
	}

	if cr.Pagination.CurrentPage == 0 {
		cr.Pagination.CurrentPage = 1
	}

	cr.Filter(c.Param("uuid"))
	c.JSON(http.StatusOK, cr)
}

// APICrawlItems API controller for crawl items
func APICrawlItems(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	cis := models.CrawlItemsSearch{}

	cis.Query.ItemsPerPage = helpers.ParseInt(c.Param("total"))
	cis.Pagination.CurrentPage = helpers.ParseInt(c.Param("page"))

	if cis.Query.ItemsPerPage == 0 {
		cis.Query.ItemsPerPage = helpers.ParseInt(envy.Get("TOTALPERPAGE", "6"))
	}

	if cis.Pagination.CurrentPage == 0 {
		cis.Pagination.CurrentPage = 1
	}

	cis.Filter(c.Param("uuid"))
	c.JSON(http.StatusOK, cis)
}

// APIProducts API crawl endpoint for products
func APIProducts(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Writer.Header().Set("x-content-type-options", "nosniff")
	c.Writer.Header().Set("x-xss-protection", "1; mode=block")
	ps := models.ProductSearch{}

	ps.Query.ItemsPerPage = helpers.ParseInt(c.Param("total"))
	ps.Pagination.CurrentPage = helpers.ParseInt(c.Param("page"))
	ps.Query.Query = c.Query("q")

	if ps.Query.ItemsPerPage == 0 {
		ps.Query.ItemsPerPage = helpers.ParseInt(envy.Get("TOTALPERPAGE", "6"))
	}

	if ps.Pagination.CurrentPage == 0 {
		ps.Pagination.CurrentPage = 1
	}

	ps.Filter(c.Request.URL)
	c.JSON(http.StatusOK, ps)
}
