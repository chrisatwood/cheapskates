// Package controllers contains all controller functions for routing for the API and the web site
package controllers

import (
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"net/http"
	"regexp"
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"bitbucket.org/chrisatwood/cheapskates/models"
	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
)

// Img used for html media source
type Img struct {
	Width  string
	Source string
}

// OfflineController is a controller for the homepage
func OfflineController(c *gin.Context) {

	hasCookie := helpers.HasCookie(c, "csc")
	c.Writer.Header().Set("Service-Worker-Allowed", "/")
	gintemplate.HTML(c, http.StatusOK, "pages/pageNotFound", gin.H{
		"title":    "Cheapskates - You're offline",
		"subtitle": "You are offline",
		"lead":     "Please check your internet connection.",
		"cookie":   hasCookie,
		"baseCSS":  helpers.GetCSS("404"),
		"bodyCSS":  "",
	})
}

// ProductSearchController handles product listing
func ProductSearchController(c *gin.Context) {
	hasCookie := helpers.HasCookie(c, "csc")
	ps := models.ProductSearch{}

	ps.Query.ItemsPerPage = helpers.ParseInt(c.Param("total"))
	ps.Pagination.CurrentPage = helpers.ParseInt(c.Param("page"))
	ps.Query.Query = c.Query("q")

	if ps.Query.ItemsPerPage == 0 {
		ps.Query.ItemsPerPage = helpers.ParseInt(envy.Get("TOTALPERPAGE", "6"))
	}

	if ps.Pagination.CurrentPage == 0 {
		ps.Pagination.CurrentPage = 1
	}

	ps.Filter(c.Request.URL)

	gintemplate.HTML(c, http.StatusOK, "pages/products", gin.H{
		"title":    "Cheapskates - Products",
		"subtitle": "You are offline",
		"lead":     "Please check your internet connection.",
		"baseCSS":  helpers.GetCSS("homepage"),
		"bodyCSS":  "",
		"Search":   ps,
		"cookie":   hasCookie,
		"Scripts":  []string{"/assets/js/lazy-loading.js"},
		"SetPadding": func(index int) string {
			classes := []string{}
			if (index+1)%2 != 0 {
				classes = append(classes, "sm:pr-2")
			} else {
				classes = append(classes, "sm:pr-0")
			}

			if (index+1)%3 != 0 {
				classes = append(classes, "md:pr-2")
			} else {
				classes = append(classes, "md:pr-0")
			}

			return strings.Join(classes, " ")
		},
		"GetSource": func(image models.ProductImage) string {
			imageSRC := ``
			if image.Source != image.OriginalSource && len(image.Source) > 0 {
				split := strings.Split(image.Source, "upload/")
				imageSRC += split[0] + `upload/e_blur:200,f_auto,fl_tiled,q_auto,w_246/` + split[1]
			} else {
				imageSRC += image.OriginalSource
			}

			return imageSRC
		},
		"GetDataSource": func(image models.ProductImage) string {
			imageSRC := ``
			if image.Source != image.OriginalSource && len(image.Source) > 0 {
				split := strings.Split(image.Source, "upload/")
				imageSRC += split[0] + `upload/f_auto,fl_tiled,q_auto,w_246/` + split[1]
			} else {
				imageSRC += image.OriginalSource
			}

			return imageSRC
		},
		"GetSrcSet": func(image models.ProductImage) []Img {
			var srcs []Img
			if image.Source != image.OriginalSource && len(image.Source) > 0 {
				split := strings.Split(image.Source, "upload/")
				srcs = append(srcs, Img{
					Width:  "570",
					Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_480/` + split[1]})
				srcs = append(srcs, Img{
					Width:  "757",
					Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_317/` + split[1]})
				srcs = append(srcs, Img{
					Width:  "967",
					Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_246/` + split[1]})

			} else {
				srcs = append(srcs, Img{Width: "110", Source: image.OriginalSource})
			}

			return srcs
		},
	})
}

// ProductViewController handles product page
func ProductViewController(c *gin.Context) {
	hasCookie := helpers.HasCookie(c, "csc")

	product := models.Product{}

	exists := product.SetByUUID(c.Param("uuid"))

	if !exists {
		c.Redirect(http.StatusSeeOther, "/page-not-found")
		c.Abort()
	} else {

		// Get list of places
		crawlItems := models.CrawlItems{}

		crawlItems.GetAllByProductID(product.ID)

		gintemplate.HTML(c, http.StatusOK, "pages/product", gin.H{
			"title":      product.Name + " - Cheapskates",
			"baseCSS":    helpers.GetCSS("product-page"),
			"bodyCSS":    "",
			"cookie":     hasCookie,
			"Product":    product,
			"CrawlItems": crawlItems,
			"Description": func(s interface{}) template.HTML {
				re := regexp.MustCompile(`/<img[^>]+>/`)
				return template.HTML(re.ReplaceAllString(html.UnescapeString(fmt.Sprint(s)), ""))
			},
			"Scripts": []string{"/assets/js/expandable.js", "/assets/js/lazy-loading.js", "/assets/js/image-gallery.js", "/assets/projects/price-history/js/manifest.js", "/assets/projects/price-history/js/vendor.js", "/assets/projects/price-history/js/app.js"},
			"GetSource": func(image models.ProductImage) string {
				imageSRC := ``
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					imageSRC += split[0] + `upload/e_blur:200,f_auto,fl_tiled,q_auto,w_400/` + split[1]
				} else {
					imageSRC += image.OriginalSource
				}

				return imageSRC
			},
			"GetDataSource": func(image models.ProductImage) string {
				imageSRC := ``
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					imageSRC += split[0] + `upload/f_auto,fl_tiled,q_auto,w_/` + split[1]
				} else {
					imageSRC += image.OriginalSource
				}

				return imageSRC
			},
			"GetSrcSet": func(image models.ProductImage) []Img {
				var srcs []Img
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					srcs = append(srcs, Img{
						Width:  "570",
						Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_481/` + split[1]})
					srcs = append(srcs, Img{
						Width:  "757",
						Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_317/` + split[1]})
					srcs = append(srcs, Img{
						Width:  "967",
						Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_389/` + split[1]})

				} else {
					srcs = append(srcs, Img{Width: "110", Source: image.OriginalSource})
				}

				return srcs
			},
			"Mod": func(i int, x int) bool {
				return i%x == 0
			},
			"GetThumbnailSource": func(image models.ProductImage) string {
				imageSRC := ``
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					imageSRC += split[0] + `upload/e_blur:200,f_auto,fl_tiled,q_auto,w_59/` + split[1]
				} else {
					imageSRC += image.OriginalSource
				}

				return imageSRC
			},
			"GetThumbnailDataSource": func(image models.ProductImage) string {
				imageSRC := ``
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					imageSRC += split[0] + `upload/f_auto,fl_tiled,q_auto,w_50/` + split[1]
				} else {
					imageSRC += image.OriginalSource
				}

				return imageSRC
			},
			"GetThumbnailSrcSet": func(image models.ProductImage) []Img {
				var srcs []Img
				if image.Source != image.OriginalSource && len(image.Source) > 0 {
					split := strings.Split(image.Source, "upload/")
					srcs = append(srcs, Img{
						Width:  "82",
						Source: split[0] + `upload/f_auto,fl_tiled,q_auto,w_82/` + split[1]})
				} else {
					srcs = append(srcs, Img{Width: "82", Source: image.OriginalSource})
				}

				return srcs
			},
			"Marshal": func(v interface{}) template.JS {
				a, _ := json.Marshal(v)
				return template.JS(a)
			},
		})
	}
}
