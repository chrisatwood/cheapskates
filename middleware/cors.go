package middleware

import (
	"time"

	"bitbucket.org/chrisatwood/cheapskates/helpers"

	"github.com/gin-gonic/gin"
)

// Cors sets cors actions
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		age := "31536000"

		expiry := time.Now().Add(time.Duration(helpers.ParseInt(age)) * time.Second)
		c.Writer.Header().Set("Cache-Control", "max-age="+age)
		c.Writer.Header().Set("x-content-type-options", "nosniff")
		c.Writer.Header().Set("x-xss-protection", "1; mode=block")
		c.Writer.Header().Set("expires", expiry.Format("Mon, 2 Jan 2022 15:04:05 GMT"))
		c.Writer.Header().Set("Service-Worker-Allowed", "/")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
