package middleware

import (
	"strings"

	"bitbucket.org/chrisatwood/cheapskates/helpers"
	"bitbucket.org/chrisatwood/cheapskates/models"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
	"github.com/google/uuid"
)

var user models.User

// Authenication only allows authenicated users to proceed
func Authenication() gin.HandlerFunc {
	return func(c *gin.Context) {
		url := envy.Get("URL", "")

		sessionValue, err := c.Cookie(envy.Get("COOKIENAME", "cookie"))
		if err == nil && sessionValue != "" {
			session := models.Session{
				Hash: sessionValue,
			}
			if session.Exists() {
				currentUA := models.UserActivity{}

				currentUA.Get(session.UserActivityID)

				userID := currentUA.UserID
				session.Delete()

				user.Get(userID)

				value := uuid.New().String()

				ua := models.UserActivity{
					User:        user,
					ActionValue: "login requested",
				}

				ua.Create("login")

				session := models.Session{
					UserActivity: ua,
					Hash:         value,
				}

				session.Create()

				c.SetCookie(envy.Get("COOKIENAME", "cookie"), value, helpers.ParseInt(envy.Get("COOKIEEXPIRY", "1800")), "/", strings.Replace(url, "https://", "", -1), false, false)

				c.Set("userUUID", user.UUID)
				c.Set("userName", user.Forename+" "+user.Surname)
				c.Next()
			} else {
				c.Redirect(302, url+"/auth/login/")
				c.Abort()
			}
		} else {
			c.Redirect(302, url+"/auth/login/")
			c.Abort()
		}
	}
}

// NotAuthenicated checks if a request user is not authenicated
func NotAuthenicated() gin.HandlerFunc {
	return func(c *gin.Context) {
		url := envy.Get("URL", "")

		sessionValue, err := c.Cookie(envy.Get("COOKIENAME", "cookie"))
		if err == nil && sessionValue != "" {

			session := models.Session{
				Hash: sessionValue,
			}

			if session.Exists() {
				c.Redirect(302, url+"/admin/")
				c.Abort()
			} else {
				c.Next()
			}

		} else {
			c.Next()
		}
	}
}
