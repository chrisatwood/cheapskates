package middleware

import (
	"bitbucket.org/chrisatwood/cheapskates/controllers"
	"github.com/gin-gonic/gin"
)

// NoRouteCheck checks if a route is valid
func NoRouteCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		status := c.Writer.Status()

		if status == 404 {
			controllers.PageNotFoundController(c)
			c.Abort()
		} else {
			c.Next()
		}
	}
}
