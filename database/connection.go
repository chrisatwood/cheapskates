// Database handles all database actions.
package database

import (
	"fmt"
	"time"

	"bitbucket.org/chrisatwood/cheapskates/helpers"

	"github.com/gobuffalo/envy"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// CreateDBConnection creates a new gorm.DB object.
func CreateDBConnection() *gorm.DB {
	db, err := gorm.Open("mysql", envy.Get("DBUSER", "root")+":"+envy.Get("DBPASSWORD", "")+"@/"+envy.Get("DBNAME", "cheapskates_dev")+"?charset="+envy.Get("DBCHARSET", "utf8")+"&parseTime="+envy.Get("DBParseTime", "True")+"&loc="+envy.Get("DBLOC", "UTC"))
	if err != nil {
		fmt.Println("Major error - Unable to connect to DB: ", err)
		time.Sleep(time.Duration(helpers.GetRandomInt(250)) * time.Millisecond)
		return CreateDBConnection()
	} else {
		db.DB().SetMaxIdleConns(100)
		db.DB().SetMaxOpenConns(1000)
		return db
	}
}

func CreateDBConnectionTest() (*gorm.DB, error) {
	db, err := gorm.Open("mysql", envy.Get("DBUSER", "root")+":"+envy.Get("DBPASSWORD", "")+"@/"+envy.Get("DBNAME", "cheapskates_dev")+"?charset="+envy.Get("DBCHARSET", "utf8")+"&parseTime="+envy.Get("DBParseTime", "True")+"&loc="+envy.Get("DBLOC", "UTC"))
	if err != nil {
		return nil, err
	}
	db.DB().SetMaxIdleConns(100)
	db.DB().SetMaxOpenConns(1000)
	return db, nil
	
}

// CloseDBConnection will close a given DB connection
func CloseDBConnection(db *gorm.DB) {
	db.Close()
}
