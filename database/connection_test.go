package database

import (
	"testing"

	"github.com/gobuffalo/envy"
)

func TestDBConnection(t *testing.T) {
	envy.Load("../.env.test")
	_, err := CreateDBConnectionTest()

	if err != nil {
		t.Errorf("Database failed to connect. Want %v, instead got %s", nil, err.Error())
	}
}
