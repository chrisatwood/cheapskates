package database

import (
	"github.com/gobuffalo/envy"
)

// GetDBTableName returns the full DB name
func GetDBTableName(items []string) string {
	name := envy.Get("DBPREFIX", "")

	for _, item := range items {
		name += envy.Get(item, "")
	}

	return name
}

// GetDBTableNameAndFeild returns the table name and a field
func GetDBTableNameAndFeild(items []string, field string) string {
	name := envy.Get("DBPREFIX", "")

	for _, item := range items {
		name += envy.Get(item, "")
	}

	if field != "" {
		name += "(" + field + ")"
	}
	return name
}
